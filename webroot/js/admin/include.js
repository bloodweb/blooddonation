var URL = 'http://' + $(location).attr('hostname') + '/blooddonation/';

function validate_user() {
    var validator = $("#userdetail").validate({
        rules: {
            name:"required",
            email: {
                required: true,
                email: true
            },
            mobile: {
                required: true,
                number: true
            },
            blood_group:"required",
            area:"required",
            location:"required"
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
    var x = validator.form();
    if(x){
        return true;
    }else{
        return false;
    }
}
function validate_bldgroup() {
    var validator = $("#bloodgrp").validate({
        rules: {
            group_name:"required",
            description:"required"
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
    var x = validator.form();
    if(x){
        return true;
    }else{
        return false;
    }
}

function changeStatus(status,id,controller) {
    if (status == 1) {
        var conf = confirm("Are You sure to inactivate the "+controller+" ?");
        var status = 2;
    } else {
        var conf = confirm("Are You sure to activate the "+controller+" ?");
        var status = 1;
    }
    if (conf) {
        if (id != '') {
            $.post(URL + "admin/"+ controller +"/changestatus", {
                "id": id,
                "status": status
            }, function (res) {
//window.location.href = "";
            });
        }
    }else{
        if(status==2){
            $("#radio5"+id).prop("checked","checked").attr('checked', 'checked');
        } else {
            $("#radio5"+id).removeAttr("checked").removeAttr('checked');
        }
        
    }
}
function insertuserid(id){
    $.post(URL+"admin/user/adduserid/",{
        "id":id
    },function(res){
        $("#retval").html(res);
        $("#myModal").openModal();
        $("#myModal").val($(this).data('id'));
        
    });

}
function validate_area() {
    var validator = $("#arealoc").validate({
        rules: {
            name:"required",
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
    var x = validator.form();
    if(x){
        return true;
    }else{
        return false;
    }
}

function validate_location() {
    var validator = $("#location").validate({
        rules: {
            name:"required",
            location: "required",
            status:"required"
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
    var x = validator.form();
    if(x){
        return true;
    }else{
        return false;
    }
}
function editbloodgroup(id){
    $.post(URL + "admin/user/editgroup", {
            "id": id,
        }, function (res) {
            if(res!=0){
               var d= $.parseJSON(res);
               $("#bld_id").val(d.id); 
               $("#name").val(d.group_name);
               $("#desc").val(d.description);
               $("#myModal").openModal();
            }
        });
}
function edituser(id){
    $.post(URL + "admin/user/edit", {
            "id": id,
        }, function (res) {
            if(res!=0){
               var d= $.parseJSON(res);
               $("#userid").val(d.id); 
               $("#username").val(d.name);
               $("#user_email").val(d.email);
               $("#mob").val(d.mobile);
               $("#bldgroup").val(d.blood_group);
               $("#user_area").val(d.area);
               $("#user_location").val(d.location);
               $("#addmyModal").openModal();
            }
        });
}
function editarea(id){
    $.post(URL + "admin/location/EditArea", {
            "id": id,
        }, function (res) {
            if(res!=0){
               var d= $.parseJSON(res);
               $("#area_id").val(d.id); 
               $("#areaname").val(d.name);
               $("#area_status").val(d.status);
               $('#editArea').modal('show');
            }
        });
}
function deletearea(id){
    $.post(URL + "admin/location/deletearea", {
            "id": id,
        }, function (res) {
        
               $("#deleteArea").openModal();
        });
}
function editLocation(id){
   
    $.post(URL + "admin/location/EditLocation", {
            "id": id,
        }, function (res) {        
            if(res!=0){
             var d= $.parseJSON(res);
             alert(d.name);
               $("#locid").val(d.id); 
               $("#area_name").val(d.name);
               $("#loc").val(d.name);
               $("#loc_status").val(d.status);
               $('#addLocation').modal('show');
            }
        });
}
function getlocation(id){
    $.post(URL+"admin/emergency/selectlocation/",{
        "id":id
    }, function(res){
        $("#user_location").html(res);
    })
}

function editemergencyreq(id){
    $.post(URL + "admin/emergency/editreq", {
            "id": id,
        }, function (res) {
            if(res!=0){
               var d= $.parseJSON(res);
               $("#req_id").val(d.id); 
               $("#username").val(d.name);
               $("#user_email").val(d.email);
               $("#mob").val(d.mobile);
               $("#bldgroup").val(d.blood_group);
               $("#user_area").val(d.area);
               $("#user_location").val(d.location);
               $("#addmyModal").openModal();
            }
        });
}


