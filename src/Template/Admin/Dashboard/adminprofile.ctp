<div class="container-fluid">
                <div class="side-body">
                    <div class="page-title">
                        <span class="title">Admin Details</span>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">Basic Information</div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <form method="post" action="<?php echo BASE_URL;?>admin/dashboard/adminprofile" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Profile</label>
                                            <input type="file" class="upload" name="profile" onchange="ValidateSingleInput(this);">
                                <input type="hidden" name="prev_img" value="<?php echo $admin['profile'];?>">
                                 <input type="hidden" name="id" value="<?php echo $admin['id'];?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">UserName</label>
                                            <p class="not-allowed" name="username"> <?php echo $admin['username'];?></p>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Password</label>
                                            <input type="password" readonly name="password" id="password" value="<?php echo $admin['password'];?>">
                                        </div>
                                         <div class="form-group">
                                            <label for="exampleInputPassword1">Name</label>
                                            <input type="text" name="name" value="<?php echo $admin['name'];?>">
                                        </div>
                                        
                                        <button type="submit" id="btn" onclick="resetpassword();">Save Changes</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>