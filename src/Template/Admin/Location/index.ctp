S<div class="container-fluid">
    <div class="side-body">
        <div class="page-title">
            <span class="title">Area & Location Management</span>
        </div>
        <!--ADD AREA-->
        <div class="row">
            <div class="col-xs-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="title">
                                Area 
                                <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#addArea">
                                    <span class="glyphicon glyphicon-plus"></span>
                                </button>
                            </div>
                        </div>                        
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sl No</th>
                                    <th><?= $this->Paginator->sort('name', ['label' => false]); ?>Area</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($area as $sl => $areaName) {?>
                                <tr>
                                    <td><?= $sl+1; ?></td>
                                    <td><?= $areaName->name; ?></td>
                                    <td>
                                        <div>
                                            <?php
                                            if($areaName->status == 0){
                                                echo "<div>
                                                        <div class='radio3 radio-check radio-success radio-inline'>
                                                            <input type='radio' id='radio5' name='radio2' value='option2' checked>
                                                            <label for='radio5'>
                                                              Active 
                                                            </label>
                                                          </div>
                                                          <div class='radio3 radio-check radio-warning radio-inline'>
                                                            <input type='radio' id='radio6' name='radio2' value='option3'>
                                                            <label for='radio6'>
                                                              Inactive
                                                            </label>
                                                          </div>
                                                        </div>";
                                                }else{
                                                echo "<div>
                                                    <div class='radio3 radio-check radio-success radio-inline'>
                                                        <input type='radio' id='radio5' name='radio2' value='option2'>
                                                        <label for='radio5'>
                                                          Active 
                                                        </label>
                                                      </div>
                                                      <div class='radio3 radio-check radio-warning radio-inline'>
                                                        <input type='radio' id='radio6' name='radio2' value='option3' checked>
                                                        <label for='radio6'>
                                                          Inactive
                                                        </label>
                                                      </div>
                                                    </div>";
                                                }
                                            ?>
                                        </div>
                                    </td>
                                    <td><a class="btn btn-xs btn-info"data-toggle="modal" data-target="#addArea" href="javascript:void(0);" onclick="editarea(<?php echo $areaName['id'];?>);">
                                            <i class="fa fa-edit"></i>


                                        <!-- <a class="btn btn-xs btn-info" data-toggle="modal" data-target="#editArea" onclick="edituserarea(<?php echo $areaName['id'];?>);"> -->
                                        </a>
                                        <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteArea"
                                        onclick="deleteuserarea(<?= $areaName['id']?>);" >
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                        <div class="pull-right">
                            <nav>
                                <ul class="pagination">
                                    <li>
                                        <a href="#" aria-label="Previous">
                                            <span aria-hidden="true">«</span>
                                        </a>
                                    </li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li>
                                        <a href="#" aria-label="Next">
                                            <span aria-hidden="true">»</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="title">
                                Location
                                <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#addLocation" >
                                    <span class="glyphicon glyphicon-plus"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sl No</th>
                                    <th>Location</th>
                                    <th>Area</th>
                                    <th>Active / Inactive</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                  <?php foreach($location as $sl => $loc) {?>
                                <tr>
                                    <td><?= $sl+1; ?></td>
                                    <td><?= $loc->name; ?></td>
                                    <td><?= $loc->parent_id; ?></td>
                                    <td>
                                        <?php
                                        if($loc->status == 1){
                                            echo "
                                                <div>
                                                    <div class='radio3 radio-check radio-success radio-inline'>
                                                        <input type='radio' id='radio7' name='radio3' value='option2'>
                                                        <label for='radio7'>
                                                          Active 
                                                        </label>
                                                      </div>
                                                      <div class='radio3 radio-check radio-warning radio-inline'>
                                                        <input type='radio' id='radio8' name='radio3' value='option3' checked>
                                                        <label for='radio8'>
                                                          Inactive
                                                        </label>
                                                      </div>
                                                </div>
                                            ";
                                        }else{
                                            echo "
                                                <div>
                                                    <div class='radio3 radio-check radio-success radio-inline'>
                                                        <input type='radio' id='radio7' name='radio3 value='option2'>
                                                        <label for='radio7'>
                                                          Active 
                                                        </label>
                                                      </div>
                                                      <div class='radio3 radio-check radio-warning radio-inline'>
                                                        <input type='radio' id='radio8' name='radio3' value='option3' checked>
                                                        <label for='radio8'>
                                                          Inactive
                                                        </label>
                                                      </div>
                                                </div>
                                            ";
                                        }
                                        ?>
                                    </td> 
                                    <td>

                                        <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#addLocation" href="javascript:void(0);" onclick="editLocation(<?=
                                        $loc['id'];?>);">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteLocation">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </td>
                                </tr>
                                 <?php }?> 
                            </tbody>
                        </table>
                        <div class="pull-right">
                            <nav>
                                <ul class="pagination">
                                    <li>
                                        <a href="#" aria-label="Previous">
                                            <span aria-hidden="true">«</span>
                                        </a>
                                    </li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li>
                                        <a href="#" aria-label="Next">
                                            <span aria-hidden="true">»</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modals -->
<!--ADD AREA MODALS-->
<div id="addArea" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" class="close">&times;</button>
                <h4 class="modal-title">Area details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form method="post" action="<?php echo BASE_URL;?>admin/location/addarea" id="arealoc" onsubmit="return validate_area();">
                        <div class="form-group">
                            <label for="">Area</label>
                            <input type="hidden" class="form-control"  name="id" id="area_id">
                            <input type="text" class="form-control" placeholder="Enter Area..." name="name" id="areaname">
                        </div>
                        <div class="form-group">
                            <label for="">Set Status</label>
                            <select name="status" id="area_status" class="form-control">
                                <option value="">Choose Status</option>
                                <option value="0">Active</option>
                                <option value="1">Inactive</option>
                            </select>
                        </div>
                        <div class="form-group text-center">
                            <button class="btn btn-info">Submit</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--DELETE AREA MODALS-->
<div id="deleteArea" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" class="close">&times;</button>
                <h4 class="modal-title">Delete <i>San Marino...</i></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group text-center">
                            <h4>
                                Are you sure want to delete this?
                            </h4>
                            <h2>
                                <span class="glyphicon glyphicon-trash"></span>
                            </h2>
                        </div>
                        <div class="form-group text-center">                            
                            <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-danger" onclick="deleteuserarea(<?= $areaName['id']?>);" 
                            >
                            Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--ADD LOCATION MODALS-->
<div id="addLocation" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" class="close">&times;</button>
                <h4 class="modal-title">Add Location</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form method="post" action="<?php echo BASE_URL;?>admin/location/addlocation" id="location" onsubmit="return validate_location();">
                        <input type="hidden" class="form-control"  name="id" id="locid" value="">

                        <div class="form-group">
                            <label for="">Area</label>
                            <select name="name" id="area_name" class="form-control">
                                <option value="">Choose Area</option>
                                <?php foreach ($area as $value) { ?>
                                        <option value="<?= $value['name']; ?>"><?= ucwords($value['name']); ?></option>
                                        <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Location</label>
                            <input type="text" class="form-control" placeholder="Enter Location..." name="name" id="loc" value="">
                        </div>
                        <div class="form-group">
                            <label for="">Set Status</label>
                            <select name="status" id="loc_status" class="form-control">
                                <option value="">Choose Status</option>
                                <option value="1">Active</option>
                                <option value="2">Inactive</option>
                            </select>
                        </div>
                        <div class="form-group text-center">
                            <button class="btn btn-info">Submit</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--EDIT LOCATION MODALS-->
<!-- <div id="editLocation" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" class="close">&times;</button>
                <h4 class="modal-title">Edit Area</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Area</label>
                            <select name="" id="" class="form-control">
                                <option value="">Choose Area</option>
                                <option value="" selected>San Marino</option>
                                <option value="">San Fransisco</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Location</label>
                            <input type="text" class="form-control" value="2nd street lane 12" placeholder="Enter Location...">
                        </div>
                        <div class="form-group">
                            <label for="">Set Status</label>
                            <select name="" id="" class="form-control">
                                <option value="">Choose Status</option>
                                <option value="" selected>Active</option>
                                <option value="">Inactive</option>
                            </select>
                        </div>
                        <div class="form-group text-center">
                            <button class="btn btn-info">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!--DELETE LOCATION MODALS-->
<div id="deleteLocation" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" class="close">&times;</button>
                <h4 class="modal-title">Delete <i>2nd street lane 12</i>...</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group text-center">
                            <h4>
                                Are you sure want to delete this?
                            </h4>
                            <h2>
                                <span class="glyphicon glyphicon-trash"></span>
                            </h2>
                        </div>
                        <div class="form-group text-center">                            
                            <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button class="btn btn-danger">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>