<div class="container-fluid">
    <div class="side-body">
        <div class="page-title">
            <span class="title">Add Location</span>
        </div>
        <div class="row">
        	<div class="col-sm-12">
        		<div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="title">
                                Enter Location Details
                            </div>
                        </div>                        
                    </div>
                    <div class="card-body">
                    	<div class="row">
                    		<div class="col-md-6 col-md-offset-3 col-sm-12">
				        		<div class="form-group">
				                    <label for="">Area</label>
				                    <select name="" id="" class="form-control">
				                        <option value="">Choose Area</option>
				                        <option value="">San Marino</option>
				                        <option value="">San Fransisco</option>
				                    </select>
				                </div>
				                <div class="form-group">
				                    <label for="">Location</label>
				                    <input type="text" class="form-control" placeholder="Enter Location...">
				                </div>
				                <div class="form-group">
				                    <label for="">Set Status</label>
				                    <select name="" id="area_status" class="form-control">
				                        <option value="">Choose Status</option>
				                        <option value="">Active</option>
				                        <option value="">Inactive</option>
				                    </select>
				                </div>
				                <div class="form-group text-center">
				                    <button class="btn btn-info">Submit</button>
				                </div>
				            </div>
				        </div>
		            </div>
		        </div>
            </div>
        </div>
    </div>
</div>