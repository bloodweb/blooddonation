<div class="container-fluid">
    <div class="side-body">
        <div class="page-title">
            <span class="title">User Management</span>
            <?= $this->Flash->render(); ?>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="card">                    
                    <div class="card-body">
                        <div class="row" style="margin-top: -35px;">
                            <div class="col-md-2 col-sm-12">
                            <br>
                                <a class="btn btn-md btn-success btn-block" href="" data-toggle="modal" data-target="#addmyModal" onclick="insertuserid();">
                                    Add User <i class="fa fa-plus" aria-hidden="true"></i>
                                </a>
                            </div>
                            <div class="col-md-7 col-sm-12 pull-right">
                                <form class="" method="get" action="<?php echo BASE_URL; ?>admin/user">
                                <div class="">                                  
                                    <div class="col-md-3 pull-right">
                                    <br>
                                        <button class="btn btn-warning btn-block" type="submit" value="Search">Search</button>
                                    </div>
                                    <div class="col-md-3 pull-right">
                                        <label for="">&nbsp;</label>
                                        <input type="text" name="location" class="form-control" placeholder="Location..." value="<?php if(isset($this->request->query['location'])){echo $this->request->query['location'];}?>">
                                    </div>
                                    <div class="col-md-3 pull-right">
                                        <label for="">&nbsp;</label>
                                        <input type="text" name="area" class="form-control" placeholder="Area..." value="<?php if(isset($this->request->query['area'])){echo $this->request->query['area'];}?>">
                                    </div>
                                    <div class="col-md-3 pull-right">
                                        <label for="">&nbsp;</label>
                                        <input type="text" class="form-control" name="name" placeholder="Name/ Email..." value="<?php if(isset($this->request->query['name'])){echo $this->request->query['name'];}?>">
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sl No</th>
                                    <th><?= $this->Paginator->sort('name', ['label' => false]); ?>Name</th>
                                    <th><?= $this->Paginator->sort('userid', ['label' => false]); ?>UserId</th>
                                    <th>Blood Group</th>
                                    <th><?= $this->Paginator->sort('email', ['label' => false]); ?>Email Id</th>
                                    <th><?= $this->Paginator->sort('mobile', ['label' => false]); ?>Phone</th>
                                    <th>Address</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if(count($user)==0){
                       echo "<tr><td colspan='8'>No Record Found</td></tr>" ;
                    }else{ 
                        foreach($user as $sl => $infodata) {?>
                                <tr>
                                    <td><?= $sl+1; ?></td>
                                    <td><?= $infodata->name; ?></td>
                                    <td><?= $infodata->userid; ?></td>
                                    <td><?= $infodata->blood_group;?></td>
                                    <td><?= $infodata->email; ?></td>
                                    <td><?= $infodata->mobile; ?></td>
                                    <td>
                                        <?= $infodata->area;?>,<?= $infodata->location;?>
                                    </td>
                                    <td>
                                        <?php $controller=$this->request->params['controller'];
                                        if($infodata['status'] == 2)
                                        {
                                        ?>
                                        <div class='radio3 radio-check radio-warning radio-inline'>
                                            <input type='radio' id="radio6" name='status' onclick="changeStatus(<?php echo $infodata['id']; ?>,<?php echo $infodata['status']; ?>,'<?= $controller; ?>');" >
                                            <label for='radio6'>
                                                Inactive
                                            </label>
                                        </div>
                                        
                                        <?php } else {?>
                                        <div class='radio3 radio-check radio-success radio-inline'>
                                            <input type='radio' id="radio5" name="status" onclick="changeStatus(<?php echo $infodata['id']; ?>,<?php echo $infodata['status']; ?>,'<?= $controller; ?>');">
                                            <label for='radio5'>
                                                Active
                                            </label>
                                        </div>
                                        <?php }?>
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-info" href="" data-toggle="modal" data-target="#addmyModal" onclick="edituser(<?php echo $infodata->id;?>);" >
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <?=
                                        $this->Form->postLink(
                                        "<i class='fa fa-trash-o'></i>",
                                        ['action' => 'delete', $infodata->id],
                                        ['escape' => false, 'title' => 'Delete', 'class' => 'btn btn-xs btn-danger', 'confirm' => __('Are you sure you want to delete user {0}?', $infodata->name)] // third
                                        );
                                        ?>
                                    </td>
                                </tr>
                                <?php } }?>
                            </tbody>
                        </table>
                        <div class="pull-right">
                            <nav>
                                <ul class="pagination">
                                    <li>
                                        <a href="#" aria-label="Previous">
                                            <span aria-hidden="true">«</span>
                                        </a>
                                    </li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li>
                                        <a href="#" aria-label="Next">
                                            <span aria-hidden="true">»</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <!-- <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('next') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div> -->
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<section class="content">
    <div id="addmyModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-info">User Details</h4>
                    <h4>UserId-<p id="retval"></p></h4>
                </div>
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="<?php echo BASE_URL;?>admin/user/adduser" id="userdetail" onsubmit="return validate_user();">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="hidden" name="id" id="userid">
                                    <input type="text" name="name" class="form-control" id="username" placeholder="Enter name" >
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="email">Email address</label>
                                    <input type="email" name="email" class="form-control" id="user_email" placeholder="Enter email">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="mobile">Mobile No</label>
                                    <input type="text" name="mobile" class="form-control" id="mob" placeholder="Enter Mobile">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label>Choose Blood Group:</label>
                                    <select class="form-control" name="blood_group" id="bldgroup">
                                        <option value=''>--Select Blood Group--</option>
                                        <?php foreach ($group as $value) { ?>
                                        <option value="<?php echo $value['group_name'];?>"><?php echo ucwords($value['group_name']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Area</label>
                                    <select class="form-control" name='area' id="user_area" onchange="getlocation(this.value);">
                                        <option value=''>--Select Area--</option>
                                        <?php foreach ($area as $value) { ?>
                                        <option value="<?php echo $value['id']; ?>"><?php echo ucwords($value['name']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Location</label>
                                    <select class="form-control" name='location' id="user_location" >
                                        <option value=''>--Select Location--</option>
                                        <?php foreach ($location as $value) { ?>
                                        <option value="<?php echo $value['id']; ?>"><?php echo ucwords($value['name']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div> 
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
