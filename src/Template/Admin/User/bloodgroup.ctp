<div class="container-fluid">
    <div class="side-body">
        <div class="page-title">
            <span class="title">Blood Group</span>
            <?= $this->Flash->render(); ?>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="card">                    
                    <div class="card-body">
                       <div class="row" style="margin-top: -35px;">
                            <div class="col-md-2 col-sm-12">
                            <br>
                                <a class="btn btn-md btn-success btn-block" href="" data-toggle="modal" data-target="#myModal">
                                    Add <i class="fa fa-plus" aria-hidden="true"></i>
                                </a>
                            </div>
                            
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sl No</th>
                                    <th>Blood Group</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($blood as $sl => $infodata) {
                                    ?>
                                <tr>
                                    <td><?= $sl+1; ?></td>
                                    <td><?= $infodata->group_name?></td>
                                    <td><?= $infodata->description?></td>
                                   <td>
                                        <div>
                                            <?php
                                            if($infodata->status == 1){
                                                echo "<div>
                                                        <div class='radio3 radio-check radio-success radio-inline'>
                                                            <input type='radio' id='radio5' name='radio2' value='option2' checked>
                                                            <label for='radio5'>
                                                              Active 
                                                            </label>
                                                          </div>
                                                          <div class='radio3 radio-check radio-warning radio-inline'>
                                                            <input type='radio' id='radio6' name='radio2' value='option3'>
                                                            <label for='radio6'>
                                                              Inactive
                                                            </label>
                                                          </div>
                                                        </div>";
                                                }else{
                                                echo "<div>
                                                    <div class='radio3 radio-check radio-success radio-inline'>
                                                        <input type='radio' id='radio5' name='radio2' value='option2'>
                                                        <label for='radio5'>
                                                          Active 
                                                        </label>
                                                      </div>
                                                      <div class='radio3 radio-check radio-warning radio-inline'>
                                                        <input type='radio' id='radio6' name='radio2' value='option3' checked>
                                                        <label for='radio6'>
                                                          Inactive
                                                        </label>
                                                      </div>
                                                    </div>";
                                                }
                                            ?>
                                        </div>
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-info" href="javascript:void(0);" data-toggle="modal" data-target="#myModal" onclick="editbloodgroup(<?php echo $infodata['id'];?>);">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <?=
                                        $this->Form->postLink(
                                        "<i class='fa fa-trash-o'></i>",
                                        ['action' => 'bldgrpdelete', $infodata->id],
                                        ['escape' => false, 'title' => 'Delete', 'class' => 'btn btn-xs btn-danger', 'confirm' => __('Are you sure you want to delete data {0}?', $infodata->group_name)] // third
                                        );
                                        ?>
                                    </td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                       <!--  <div class="pull-right">
                            <nav>
                                <ul class="pagination">
                                    <li>
                                        <a href="#" aria-label="Previous">
                                            <span aria-hidden="true">«</span>
                                        </a>
                                    </li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li>
                                        <a href="#" aria-label="Next">
                                            <span aria-hidden="true">»</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div> -->
                        <!-- <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('next') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div> -->
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<section class="content">
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-info">Blood Details</h4>

                </div>
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="<?php echo BASE_URL;?>admin/user/blood" id="bloodgrp" onsubmit="return validate_bldgroup();">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="name">Blood Group</label>
                                     <input type="hidden" name="id" id="bld_id">
                                    <input type="text" name="group_name" class="form-control" id="name" placeholder="Enter name">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="name">Description</label>
                                    <textarea name="description" class="form-control" id="desc" placeholder="Enter description">
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>