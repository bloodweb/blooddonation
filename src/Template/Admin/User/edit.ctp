<div class="container-fluid">
                <div class="side-body">
                    <div class="page-title">
                        <span class="title">Edit User</span>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <div class="title">Basic Information</div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <form method="post" action="<?php echo BASE_URL;?>admin/user/adduser/<?= $userlist->id?>">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Name</label>
                                             <input type="hidden" value="<?php if(!empty($userlist)){ echo $userlist['id'];}?>" name="id">
                                            <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter name" value="<?php if(!empty($userlist)){ echo $userlist['name'];}?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email address</label>
                                            <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" value="<?php if(!empty($userlist)){ echo $userlist['email'];}?>">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Mobile No</label>
                                            <input type="text" name="mobile" class="form-control" id="exampleInputPassword1" placeholder="Enter Mobile" value="<?php if(!empty($userlist)){ echo $userlist['mobile'];}?>">
                                        </div>
                                        
                                         <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label>Choose Blood Group:</label>
                                    <select class="form-control" name="blood_group">
                                        <option value=''>--Select Blood Group--</option>
                                        <?php foreach ($group as $value) { ?>
                                        <option value="<?php echo $value['group_name']; ?>"<?php
                                        if (!empty($userlist)) {
                                            if ($userlist['blood_group'] == $value->group_name) {
                                                echo "selected";
                                            }
                                        }
                                            ?>><?php echo ucwords($value['group_name']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Area</label>
                                    <select class="form-control" name='area' id="section_name" >
                                        <option value=''>--Select Area--</option>
                                        <?php foreach ($area as $value) { ?>
                                        <option value="<?php echo $value['name']; ?>" <?php if (!empty($userlist)) {
                                            if ($userlist['area']==$value['name']) {
                                                echo "selected";
                                            }
                                        }?>><?php echo ucwords($value['name']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Location</label>
                                    <select class="form-control" name='location' id="section_name" >
                                        <option value=''>--Select Location--</option>
                                        <?php foreach ($location as $value) { ?>
                                        <option value="<?php echo $value['name']; ?>" <?php if (!empty($userlist)) {
                                            if ($userlist['location']==$value['name']) {
                                                echo "selected";
                                            }
                                        }?>><?php echo ucwords($value['name']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div> 
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>   
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>