<div class="container-fluid">
    <div class="side-body">
        <div class="page-title">
            <span class="title">Emergency Management</span>
            <?= $this->Flash->render(); ?>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="card">                    
                    <div class="card-body">
                         <div class="col-md-2 col-sm-12">
                            <br>
                                <a class="btn btn-md btn-success btn-block" href="" data-toggle="modal" data-target="#addmyModal">
                                    Add <i class="fa fa-plus" aria-hidden="true"></i>
                                </a>
                            </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sl No</th>
                                    <th><?= $this->Paginator->sort('title', ['label' => false]); ?>Title</th>
                                    <th><?= $this->Paginator->sort('phone', ['label' => false]); ?>Mobile</th>
                                    <th>Blood Group</th>
                                    <th><?= $this->Paginator->sort('area', ['label' => false]); ?>Area</th>
                                    <th><?= $this->Paginator->sort('location', ['label' => false]); ?>Location</th>
                                    <th>Description</th>
                                    <th><?= $this->Paginator->sort('required_date', ['label' => false]); ?>Required Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if(count($data)==0){
                       echo "<tr><td colspan='8'>No Record Found</td></tr>" ;
                    }else{ 
                        foreach($data as $sl => $infodata) {?>
                                <tr>
                                    <td><?= $sl+1; ?></td>
                                    <td><?= $infodata->title; ?></td>
                                    <td><?= $infodata->phone; ?></td>
                                    <td><?= $infodata->blood_group; ?></td>
                                    <td><?= $infodata->area;?></td>
                                    <td><?= $infodata->location; ?></td>
                                    <td><?= $infodata->description; ?></td>
                                    <td><?= $infodata->required_date; ?></td>
                                    <td>
                                        <?php $controller=$this->request->params['controller'];
                                        if($infodata['status'] == 2)
                                        {
                                        ?>
                                        <div class='radio3 radio-check radio-warning radio-inline'>
                                            <input type='radio' id="radio6" name='status' onclick="changeStatus(<?php echo $infodata['id']; ?>,<?php echo $infodata['status']; ?>,'<?= $controller; ?>');" >
                                            <label for='radio6'>
                                                Inactive
                                            </label>
                                        </div>
                                        
                                        <?php } else {?>
                                        <div class='radio3 radio-check radio-success radio-inline'>
                                            <input type='radio' id="radio5" name="status" onclick="changeStatus(<?php echo $infodata['id']; ?>,<?php echo $infodata['status']; ?>,'<?= $controller; ?>');">
                                            <label for='radio5'>
                                                Active
                                            </label>
                                        </div>
                                        <?php }?>
                                    </td>
                                    <td>
                                        <a class="btn btn-xs btn-info" href="" data-toggle="modal" data-target="#addmyModal" onclick="editemergencyreq(<?php echo $infodata->id;?>);" >
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <?=
                                        $this->Form->postLink(
                                        "<i class='fa fa-trash-o'></i>",
                                        ['action' => 'delete', $infodata->id],
                                        ['escape' => false, 'title' => 'Delete', 'class' => 'btn btn-xs btn-danger', 'confirm' => __('Are you sure you want to delete user {0}?', $infodata->name)] // third
                                        );
                                        ?>
                                          <a href="javascript:void(0)" title="Send Notification" class="btn btn-xs btn-info" data-toggle="modal" data-target="#notificationmodal">
                                                    <i class="fa fa-bell-o" aria-hidden="true"></i>
                                                </a>  
                                    </td>
                                </tr>
                                <?php } }?>
                            </tbody>
                        </table>
                        <div class="pull-right">
                            <nav>
                                <ul class="pagination">
                                    <li>
                                        <a href="#" aria-label="Previous">
                                            <span aria-hidden="true">«</span>
                                        </a>
                                    </li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li>
                                        <a href="#" aria-label="Next">
                                            <span aria-hidden="true">»</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <!-- <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('next') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div> -->
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<section class="content">
    <div id="addmyModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-info">Req Details</h4>
                    
                </div>
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="<?php echo BASE_URL;?>admin/emergency/add" id="userdetail">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="name">Title</label>
                                    <input type="hidden" name="id" id="req_id">
                                    <input type="text" name="title" class="form-control" id="req_title" placeholder="Enter name" >
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="email">Phone</label>
                                    <input type="text" name="phone" class="form-control" id="req_phone" placeholder="Enter email">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Area</label>
                                    <select class="form-control" name='area' id="req_area" onchange="getlocation(this.value);" >
                                        <option value=''>--Select Area--</option>
                                        <?php foreach ($area as $value) { ?>
                                        <option value="<?php echo $value['id']; ?>"><?php echo ucwords($value['name']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Location</label>
                                    <select class="form-control" name='location' id="req_location" >
                                        <option value=''>--Select Location--</option>
                                        <?php foreach ($location as $value) { ?>
                                        <option value="<?php echo $value['id']; ?>"><?php echo ucwords($value['name']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div> 
                            </div>
                             <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label>Choose Blood Group:</label>
                                    <select class="form-control" name="blood_group" id="req_bldgroup">
                                        <option value=''>--Select Blood Group--</option>
                                        <?php foreach ($group as $value) { ?>
                                        <option value="<?php echo $value['group_name'];?>"><?php echo ucwords($value['group_name']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="email">Required Date</label>
                                    <input type="text" name="required_date" class="form-control" id="req_date" placeholder="Enter email">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="mobile">Description</label>
                                    <textarea  name="description" class="form-control" id="req_desc" placeholder="Enter Mobile">
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div id="notificationmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-info">Emergency Notification</h4>
                    
                </div>
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="<?php echo BASE_URL;?>admin/emergency/emergencynotification" id="userdetail">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="name">Title</label>
                                    <input type="hidden" name="id" id="noti_id">
                                    <input type="text" name="title" class="form-control" id="username" placeholder="Enter name" >
                                </div>
                                <div class="form-group">
                                    <label for="name">Title</label>
                                    <textarea class="form-control" id="username" placeholder="Enter name" name="notification" >
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


