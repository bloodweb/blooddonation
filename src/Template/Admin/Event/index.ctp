<div class="container-fluid">
    <div class="side-body">
        <div class="page-title">
            <span class="title">Event Management</span>
            <?= $this->Flash->render(); ?>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="card">                    
                    <div class="card-body">
                         <div class="col-md-2 col-sm-12">
                            <br>
                                <a class="btn btn-md btn-success btn-block" href="" data-toggle="modal" data-target="#addmyModal">
                                    Add <i class="fa fa-plus" aria-hidden="true"></i>
                                </a>
                            </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                              	    <th>SlNo</th>
                                    <th>Title</th>
                                    <th>Area</th>
                                    <th>Location</th>
                                    <th>EventBanner</th>
                                    <th>EventDate</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr><?php foreach($events as $sl => $event) {?>
                                    <td><?= $sl+1; ?></td>
                                    <td><?= $event->title?></td>
                                    <td><?= $area[$event->area];?></td>
                                    <td><?= $location->name ?></td>
                                    <td><?= $event->eventbanner?></td>
                                    <td><?= date("jS M Y",strtotime($event->eventdate));?></td>
                                 
                                    <td>
                                        <div class='radio3 radio-check radio-warning radio-inline'>
                                            <input type='radio' id="radio6" name='status' onclick="" >
                                            <label for='radio6'>
                                                Inactive
                                            </label>
                                        </div>
                                        <div class='radio3 radio-check radio-success radio-inline'>
                                            <input type='radio' id="radio5" name="status" onclick="">
                                            <label for='radio5'>
                                                Active
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                       <a class="btn btn-xs btn-info" href="" data-toggle="modal" data-target="#addmyModal" onclick="editevent(<?php echo $event->id;?>);" >
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <?=
                                        $this->Form->postLink(
                                        "<i class='fa fa-trash-o'></i>",
                                        ['action' => 'delete', $event->id],
                                        ['escape' => false, 'title' => 'Delete', 'class' => 'btn btn-xs btn-danger', 'confirm' => __('Are you sure you want to delete event {0}?', $event->title)] // third
                                        );
                                        ?>
                                    </td>
                                </tr>
                              <?php } ?>
                            </tbody>
                        </table>
                        <div class="pull-right">
                            <nav>
                                <ul class="pagination">
                                    <li>
                                        <a href="#" aria-label="Previous">
                                            <span aria-hidden="true">«</span>
                                        </a>
                                    </li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li>
                                        <a href="#" aria-label="Next">
                                            <span aria-hidden="true">»</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <!-- <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                                <?= $this->Paginator->numbers() ?>
                                <?= $this->Paginator->next(__('next') . ' >') ?>
                            </ul>
                            <p><?= $this->Paginator->counter() ?></p>
                        </div> -->
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<section class="content">
    <div id="addmyModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-info">Add Event Details</h4>
                    
                </div>
                <div class="card">
                    <div class="card-body">
                        <form method="post" action="<?php echo BASE_URL;?>admin/event/addevent" id="userdetail" enctype="multipart/form-data">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="name">Title</label>
                                    <input type="hidden" name="id" id="event_id">
                                    <input type="text" name="title" class="form-control" id="event_title" placeholder="Enter name" >
                                </div>
                            </div>
                               <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Area</label>
                                    <select class="form-control" name='area' id="req_area" onchange="getlocation(this.value);" >
                                        <option value=''>--Select Area--</option>
                                        <?php foreach ($area as $value) { pr($value); exit; ?>
                                        <option value="<?php echo $value['name']; ?>"><?php echo ucwords($value['name']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Location</label>
                                    <select class="form-control" name='location' id="event_location" >
                                        <option value=''>--Select Location--</option>
                                        <?php foreach ($location as $value) { ?>
                                        <option value="<?php echo $value['name']; ?>"><?php echo ucwords($value['name']); ?></option>
                                        <?php } ?>
                                    </select>
                                </div> 
                            </div>
                             <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label>Upload Event Banner</label>
                                       <input type="file" id="exampleInputFile" class="form-control" name="eventbanner">

                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="email">Required Date</label>
                                    <input type="date" name="eventdate" class="form-control" id="event_date" placeholder="Enter email">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="mobile">Description</label>
                                    <textarea  name="description" class="form-control" id="event_desc" placeholder="Enter Mobile">
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>