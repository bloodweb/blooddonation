<div class="header_banner">
				<div class="bs-search">
					<h4>Getting blood is now just a click away!!!</h4>
					<div>
						<select name="" id="">
							<option value="">You're...</option>
							<option value="">Recipient</option>
							<option value="">Donor</option>
						</select>
					</div>
					<div>
						<select name="group_name" id="">
							<option value="">Select Blood Group</option>
							<?php foreach ($group as $value) { ?>
                                        <option value="<?php echo $value['group_name'];?>"><?php echo ucwords($value['group_name']); ?></option>
                                        <?php } ?>
						</select>
					</div>
					<div>
						<select name="name">
					    	<option value="">Select Area</option>
							<?php foreach ($area as $value) { ?>
                                        <option value="<?php echo $value['name']; ?>"><?php echo ucwords($value['name']); ?></option>
                                        <?php } ?>
					    </select>
					</div>
					<div>
						<button onclick="window.location.href='<?= BASE_URL."Index/searchresult"?>'">
							<span class="glyphicon glyphicon-search" aria-hidden="true"></span> Search
						</button>
					</div>
				</div>
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
					<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
					</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<div class="item active  image-wid">
					<img src="<?php echo BASE_URL;?>images/sl3.png" alt="..." class="img-responsive">
					</div>
					<div class="item  image-wid">
					<img src="<?php echo BASE_URL;?>images/sl2.png" alt="..." class="img-responsive">
					</div>
					<div class="item  image-wid">
					<img src="<?php echo BASE_URL;?>images/sl1.png" alt="..." class="img-responsive">
					</div>
				</div>
				<!-- Controls -->
				<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
				</a>
				</div>
			</div>
<div class="style-label">
			<div class="container">
				<ul class="box-shadow effect2">
					<div class="col-md-12 ser-fet">
						<br>
						<h3>Why Blood Share?</h3>
					</div>
					<li class="col-md-3">
						<span class="glyphicon glyphicon-leaf flt" aria-hidden="true"></span>
						<div class="label-text">
						<h3>Clean</h3>
						<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
						</div>
					</li>
					<li class="col-md-3">
						<span class="glyphicon glyphicon-eye-open flt" aria-hidden="true"></span>
						<div class="label-text">
						<h3>Focusing</h3>
						<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
						</div>
					</li>
					<li class="col-md-3">
						<span class="glyphicon glyphicon-pencil flt" aria-hidden="true"></span>
						<div class="label-text">
						<h3>Prescription</h3>
						<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
						</div>
					</li>
					<li class="col-md-3">
						<span class="glyphicon glyphicon-cutlery flt" aria-hidden="true"></span>
						<div class="label-text">
						<h3>Diet</h3>
						<p>Lorem Ipsum is simply dummy text of the printing industry.</p>
						</div>
					</li>
					<div class="clearfix"></div>
				</ul>
			</div>
		</div>
		<div class="content">
			<div class="service_features" id="features">
				<div class="container">
					<div class="col-md-4 ser-fet">
						<h3>Our Services</h3>
						<p>We are good at</p>
						<span class="line"></span>
						<div class="services">
							<div class="menu-grid">
								<ul class="menu_drop">
									<li class="item1 plus"><a href="#" class="active">Therapy<span class="caret"></span></a>
										<ul>
											<li class="subitem1">
												<p> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
												Lorem Ipsum is simply dummy text of the printing and typesetting industry</p><br>
												<p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
												It has survived not only five centuries, but also the leap into electronic typesetting</p>
											</li>
										</ul>
									</li>
									<li class="item3 plus"><a href="#" class="active">Orthopedic<span class="caret"></span></a>
										<ul>
											<li class="subitem1">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry,
												Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p><br>
												<p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
												It has survived not only five centuries, but also the leap into electronic typesetting</p>
											</li>
										</ul>
									</li>
									<li class="item4 plus"><a href="#" class="active">Heart specialist<span class="caret"></span></a>
										<ul>
											<li class="subitem1">
												<p> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
												Lorem Ipsum is simply dummy text of the printing and typesetting industry</p><br>
												<p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
												It has survived not only five centuries, but also the leap into electronic typesetting</p>
											</li>
										</ul>
									</li>
									<li class="item4 plus"><a href="#" class="active">kidney & Liver<span class="caret"></span></a>
										<ul>
											<li class="subitem1">
												<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
												Lorem Ipsum is simply dummy text of the printing and typesetting industry</p><br>
												<p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
												It has survived not only five centuries, but also the leap into electronic typesetting</p>
											</li>
										</ul>
									</li>
									<li class="item5 plus"><a href="#" class="active">Cancer Cell<span class="caret"></span></a>
										<ul>
											<li class="subitem1">
												<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
												Lorem Ipsum is simply dummy text of the printing and typesetting industry</p><br>
												<p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
												It has survived not only five centuries, but also the leap into electronic typesetting</p>
											</li>
										</ul>
									</li>
									<li class="item6 plus"><a href="#" class="active">Psychiatric Cell<span class="caret"></span></a>
										<ul>
											<li class="subitem1">
												<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
												Lorem Ipsum is simply dummy text of the printing and typesetting industry</p><br>
												<p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
												It has survived not only five centuries, but also the leap into electronic typesetting</p>
											</li>
										</ul>
									</li>
								</ul>
								<!-- script for tabs -->
								<script type="text/javascript">
									$(function() {
										var menu_ul = $('.menu_drop > li > ul'),
											menu_a  = $('.menu_drop > li > a');
												menu_ul.hide();
													menu_a.click(function(e) {
													e.preventDefault();
													if(!$(this).hasClass('active')) {
													menu_a.removeClass('active');
													menu_ul.filter(':visible').slideUp('normal');
													$(this).addClass('active').next().stop(true,true).slideDown('normal');
													} else {
													$(this).removeClass('active');
												$(this).next().stop(true,true).slideUp('normal');
											}
										});
									});
								</script>
							<!-- script for tabs -->
						</div>
						</div>
					</div>
					<div class="col-md-8 ser-fet">
						<h3>Our Features</h3>
						<p>Our Aim</p>
						<span class="line"></span>
						<div class="features">
							<div class="col-md-6 fet-pad">
								<div class="div-margin">
									<div class="col-md-3 fet-pad wid">
										<span class="glyphicon glyphicon-user aim-icn" aria-hidden="true"></span>
									</div>
									<div class="col-md-9 fet-pad wid2">
										<h4>Patient Profile</h4>
										<p>Lorem Ipsum is simply dummy text of the printing.</p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="div-margin">
									<div class="col-md-3 fet-pad wid">
										<span class="glyphicon glyphicon-screenshot aim-icn" aria-hidden="true"></span>
									</div>
									<div class="col-md-9 fet-pad wid2">
										<h4>Advanced Equipment</h4>
										<p>Lorem Ipsum is simply dummy text of the printing.</p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="div-margin">
									<div class="col-md-3 fet-pad wid">
										<span class="glyphicon glyphicon-ok aim-icn" aria-hidden="true"></span>
									</div>
									<div class="col-md-9 fet-pad wid2">
										<h4>Operations sucessed</h4>
										<p>Lorem Ipsum is simply dummy text of the printing.</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-6 fet-pad">
								<div class="div-margin">
									<div class="col-md-3 fet-pad wid">
										<span class="glyphicon glyphicon-piggy-bank aim-icn" aria-hidden="true"></span>
									</div>
									<div class="col-md-9 fet-pad wid2">
										<h4>Health Insurance</h4>
										<p>Lorem Ipsum is simply dummy text of the printing.</p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="div-margin">
									<div class="col-md-3 fet-pad wid">
										<span class="glyphicon glyphicon-education aim-icn" aria-hidden="true"></span>
									</div>
									<div class="col-md-9 fet-pad wid2">
										<h4>Recognised Doctors</h4>
										<p>Lorem Ipsum is simply dummy text of the printing.</p>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="div-margin">
									<div class="col-md-3 fet-pad wid">
										<span class="glyphicon glyphicon-heart aim-icn" aria-hidden="true"></span>
									</div>
									<div class="col-md-9 fet-pad wid2">
										<h4>Satisfaction</h4>
										<p>Lorem Ipsum is simply dummy text of the printing.</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="acheivments" id="acheive">
			<div class="container top">
					<h3>Current Events</h3>
					<!--script-->
					<?= $this->Html->css('swipebox.css'); ?>
					<?= $this->Html->script('jquery.swipebox.min.js');?>
					<script type="text/javascript">
								jQuery(function($) {
									$(".swipebox").swipebox();
								});
					</script>
					<!--script-->
					<div class="gallery-grids">
						<ul>
							<li class="col-md-4 gal-alt">
								<a href="<?php echo BASE_URL;?>images/1i.jpg" class="swipebox">
								<img src="<?php echo BASE_URL;?>images/1i.jpg" alt="/"/>
									<span class="hide-box">
										<h4>Neutron collider Microscope</h4>
										<p>Great transparency Rate</p>
									</span>
							</li></a>
							<li class="col-md-4 gal-alt">
								<a href="<?php echo BASE_URL;?>images/1p.jpg" class="swipebox">
								<img src="<?php echo BASE_URL;?>images/1p.jpg" alt="/"/>
									<span class="hide-box">
										<h4>German made Ak-2000 Ct-Scan</h4>
										<p>Lorem is a dummy Text</p>
									</span>
							</li></a>
							<li class="col-md-4 gal-alt">
								<a href="<?php echo BASE_URL;?>images/1n.jpg" class="swipebox">
								<img src="<?php echo BASE_URL;?>images/1n.jpg" alt="/" />
								<span class="hide-box">
										<h4>Symbiosis Laser T-300</h4>
										<p>Lorem is a dummy Text</p>
								</span>
							</li></a>
							<li class="col-md-4 gal-alt">
								<a href="<?php echo BASE_URL;?>images/1k.jpg" class="swipebox">
								<img src="<?php echo BASE_URL;?>images/1k.jpg" alt="/" />
								<span class="hide-box">
										<h4>Angeo Blast</h4>
										<p>Lorem is a dummy Text</p>
								</span>	
							</li></a>
							<li class="col-md-4 gal-alt">
								<a href="<?php echo BASE_URL;?>images/1o.jpg" class="swipebox">
								<img src="<?php echo BASE_URL;?>images/1o.jpg" alt="/" />
								<span class="hide-box">
										<h4>Spacious Operatio Theater</h4>
										<p>Lorem is a dummy Text</p>
								</span>
							</li></a>
							<li class="col-md-4 gal-alt">
								<a href="<?php echo BASE_URL;?>images/1f.jpg" class="swipebox">
								<img src="<?php echo BASE_URL;?>images/1f.jpg" alt="/"/>
								<span class="hide-box">
										<h4>Latest Ventilators</h4>
										<p>Lorem is a dummy Text</p>
								</span>
							</li></a>
							<div class="clearfix"></div>
						</ul>
					</div>
			</div>
			</div>
		</div>
		<div class="doctor-port" id="doc">
			<h3>Sharing Blood Was Never That Easy</h3>
			<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-12">
					<img src="<?php echo BASE_URL;?>images/phones.png" alt="" class="img-responsive">
				</div>
				<div class="col-md-8 col-sm-12">
					<h4 class="text-left">
						Download India's #1 blood sharing app and stay healthy! 
						<br>
						It's free, easy and smart.
					</h4>
					<br>
					<a href="https://play.google.com/store?hl=en" class="pull-left">
						<img src="http://www.almanhal.com/ResourceImages/Publisher/StartigicPublisherLogos/GooglePlay.png" alt="" style="height: 50px;">
					</a>
					<div class="clearfix"></div>
					<p class="text-left">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit veniam provident temporibus neque, facere consequuntur tempora nostrum sequi eius iure, voluptatem ipsam nam soluta id cum ad sit reiciendis totam!
					</p>
					<br>
					<p class="text-left">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit veniam provident temporibus neque, facere consequuntur tempora nostrum sequi eius iure, voluptatem ipsam nam soluta id cum ad sit reiciendis totam!
					</p>
					<br>
					<p class="text-left">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit veniam provident temporibus neque, facere consequuntur tempora nostrum sequi eius iure, voluptatem ipsam nam soluta id cum ad sit reiciendis totam!
					</p>
					<br>
					<p class="text-left">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit veniam provident temporibus neque, facere consequuntur tempora nostrum sequi eius iure, voluptatem ipsam nam soluta id cum ad sit reiciendis totam!
					</p>
				</div>				
			</div>
			</div>
		</div>
		<?= $this->element('index_footer');?>
		<div class="contact">
			<div class="map">
				<iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d190028.76432096347!2d12.535997899999987!3d41.91007110000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132f6196f9928ebb%3A0xb90f770693656e38!2sRome%2C+Italy!5e0!3m2!1sen!2sin!4v1436158065010" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		
		</div>