	<div class="content">
			<div class="service_features" id="features">
				<div class="container">
					<div class="well">
						<div class="row">
							<div class="col-sm-12 ser-fet">
								<p>Search...</p>
							</div>
							<div class="col-md-3 col-sm-12">
								<div class="form-group">
								    <select class="form-control">
								    	<option value="">You're...</option>
										<option value="">Recipient</option>
										<option value="">Donor</option>
								    </select>
							  	</div>
							</div>
							<div class="col-md-3 col-sm-12">
								<div class="form-group">
								    <select class="form-control">
								    	<option value="">Select Blood Group</option>
										<?php foreach ($group as $value) { ?>
                                        <option value="<?php echo $value['group_name'];?>"><?php echo ucwords($value['group_name']); ?></option>
                                        <?php } ?>
								    </select>
							  	</div>
							</div>
							<div class="col-md-3 col-sm-12">
								<div class="form-group">
									<select class="form-control">
								    	<option value="">Select Area</option>
										<?php foreach ($area as $value) { ?>
                                        <option value="<?php echo $value['name']; ?>"><?php echo ucwords($value['name']); ?></option>
                                        <?php } ?>
								    </select>
								</div>
							</div>
							<div class="col-md-3 col-sm-12">
								<input type="submit" class="btn btn-block btn-danger" value="Search">
							</div>
						</div>
					</div>
					
					<div class="col-md-8 ser-fet">
						<h3>All Donors</h3>
						<p>You have searched for <b>O+</b> near <b>Chicago</b></p>
						<span class="line"></span>
						<div class="features">
							<div class="row">
								<div class="col-sm-12">
									<div class="bs-box">
										<div class="bs-img-box">
											<div class="drop"><span>2</span></div>
											<img src="<?php echo BASE_URL;?>images/a5.jpg" alt="" class="bs-img">
										</div>
										<div class="bs-details">
											<h4>Jhon Doe, <span> <span class="glyphicon glyphicon-map-marker"></span> San Marino, Block 82</span></h4>
											<p>
												Blood Group: O+ &nbsp;&nbsp;&nbsp;&nbsp;
												<small>
													<span class="glyphicon glyphicon-star"></span>
													<span class="glyphicon glyphicon-star"></span>
													<span class="glyphicon glyphicon-star"></span>
													<span class="glyphicon glyphicon-star"></span>
												</small>
											</p>
											<p>
												<small>
													<i>
														Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde veritatis repellendus velit beatae modi voluptatibus dolore, earum natus nobis perferendis inventore quia architecto, fuga aperiam quidem possimus ratione. Molestiae, enim.
													</i>
												</small>
											</p>
											<hr>
											<button>Request</button>
										</div>
									</div>
									<div class="bs-box">										
										<div class="bs-img-box">
											<div class="drop"><span>12</span></div>
											<img src="<?php echo BASE_URL;?>images/a5.jpg" alt="" class="bs-img">
										</div>
										<div class="bs-details">
											<h4>Jhon Doe, <span> <span class="glyphicon glyphicon-map-marker"></span> San Marino, Block 82</span></h4>
											<p>
												Blood Group: O+ &nbsp;&nbsp;&nbsp;&nbsp;
												<small>
													<span class="glyphicon glyphicon-star"></span>
													<span class="glyphicon glyphicon-star"></span>
													<span class="glyphicon glyphicon-star"></span>
													<span class="glyphicon glyphicon-star"></span>
												</small>
											</p>
											<p>
												<small>
													<i>
														Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde veritatis repellendus velit beatae modi voluptatibus dolore, earum natus nobis perferendis inventore quia architecto, fuga aperiam quidem possimus ratione. Molestiae, enim.
													</i>
												</small>
											</p>
											<hr>
											<button>Request</button>
										</div>
									</div>
									<div class="bs-box">										
										<div class="bs-img-box">
											<div class="drop"><span>5</span></div>
											<img src="<?php echo BASE_URL;?>images/a5.jpg" alt="" class="bs-img">
										</div>
										<div class="bs-details">
											<h4>Jhon Doe, <span> <span class="glyphicon glyphicon-map-marker"></span> San Marino, Block 82</span></h4>
											<p>
												Blood Group: O+ &nbsp;&nbsp;&nbsp;&nbsp;
												<small>
													<span class="glyphicon glyphicon-star"></span>
													<span class="glyphicon glyphicon-star"></span>
													<span class="glyphicon glyphicon-star"></span>
													<span class="glyphicon glyphicon-star"></span>
												</small>
											</p>
											<p>
												<small>
													<i>
														Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde veritatis repellendus velit beatae modi voluptatibus dolore, earum natus nobis perferendis inventore quia architecto, fuga aperiam quidem possimus ratione. Molestiae, enim.
													</i>
												</small>
											</p>
											<hr>											
											<button>Request</button>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<?= $this->element('index_footer');?>