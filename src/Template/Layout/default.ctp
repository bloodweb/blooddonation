<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
    <?= $this->element('header');?>
    <body class="flat-blue">
        <div class="app-container">
            <div class="row content-container">
                <nav class="navbar navbar-default navbar-fixed-top navbar-top">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-expand-toggle">
                                <i class="fa fa-bars icon"></i>
                            </button>
                            <ol class="breadcrumb navbar-breadcrumb">
                                <li class="active">Dashboard</li>
                            </ol>
                            <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                                <i class="fa fa-th icon"></i>
                            </button>
                        </div>
                        <ul class="nav navbar-nav navbar-right">
                            <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                                <i class="fa fa-times icon"></i>
                            </button>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-comments-o"></i></a>
                                <ul class="dropdown-menu animated fadeInDown">
                                    <li class="title">
                                        Notification <span class="badge pull-right">0</span>
                                    </li>
                                    <li class="message">
                                        No new notification
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown danger">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-star-half-o"></i> 4</a>
                                <ul class="dropdown-menu danger  animated fadeInDown">
                                    <li class="title">
                                        Notification <span class="badge pull-right">4</span>
                                    </li>
                                    <li>
                                        <ul class="list-group notifications">
                                            <a href="#">
                                                <li class="list-group-item">
                                                    <span class="badge">1</span> <i class="fa fa-exclamation-circle icon"></i> new registration
                                                </li>
                                            </a>
                                            <a href="#">
                                                <li class="list-group-item">
                                                    <span class="badge success">1</span> <i class="fa fa-check icon"></i> new orders
                                                </li>
                                            </a>
                                            <a href="#">
                                                <li class="list-group-item">
                                                    <span class="badge danger">2</span> <i class="fa fa-comments icon"></i> customers messages
                                                </li>
                                            </a>
                                            <a href="#">
                                                <li class="list-group-item message">
                                                    view all
                                                </li>
                                            </a>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown profile">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Emily Hart <span class="caret"></span></a>
                                <ul class="dropdown-menu animated fadeInDown">
                                    <li class="profile-img">
                                        <img src="<?php echo BASE_URL;?>images/profile/picjumbo.com_HNCK4153_resize.jpg" class="profile-img">
                                    </li>
                                    <li>
                                        <div class="profile-info">
                                            <h4 class="username">Emily Hart</h4>
                                            <p>emily_hart@email.com</p>
                                            <div class="btn-group margin-bottom-2x" role="group">
                                                <a href="<?php echo BASE_URL;?>admin/dashboard/adminprofile" class="btn btn-default btn-flat"><i class="fa fa-user"></i> Profile</a>
                                                <a href="<?php echo BASE_URL;?>admin/dashboard/logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i>Sign out</a>
                                                
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="side-menu sidebar-inverse">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="side-menu-container">
                            <div class="navbar-header">
                                <a class="navbar-brand" href="#">
                                    <div class="icon fa fa-paper-plane"></div>
                                    <div class="title">Flat Admin V.2</div>
                                </a>
                                <button type="button" class="navbar-expand-toggle pull-right visible-xs">
                                    <i class="fa fa-times icon"></i>
                                </button>
                            </div>
                            <ul class="nav navbar-nav">
                                <li class="active">
                                    <a href="<?php echo BASE_URL;?>admin/dashboard/">
                                        <span class="icon fa fa-tachometer"></span><span class="title">Dashboard</span>
                                    </a>
                                </li>
                                <li class="panel panel-default dropdown">
                                    <a data-toggle="collapse" href="#dropdown-element">
                                        <span class="icon fa fa-desktop"></span><span class="title">User Management</span>
                                    </a>
                                    <!-- Dropdown level 1 -->
                                    <div id="dropdown-element" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul class="nav navbar-nav">
                                                <li><a href="<?php echo BASE_URL;?>admin/user">User List</a>
                                                </li>
                                                 <li><a href="<?php echo BASE_URL;?>admin/user/userating">User Rating</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                 <li class="panel panel-default dropdown">
                                    <a href="<?php echo BASE_URL;?>admin/user/bloodgroup">
                                         <span class="icon fa fa-file-text-o"></span><span class="title">Blood Group Management</span>
                                    </a>
                                </li>
                                 <li class="panel panel-default dropdown">
                                    <a href="<?php echo BASE_URL;?>admin/location">
                                        <span class="icon fa fa-table"></span><span class="title">Location Management</span>
                                    </a>
                                </li>
                                 <li class="panel panel-default dropdown">
                                    <a data-toggle="collapse" href="#dropdown-form">
                                        <span class="icon fa fa-file-text-o"></span><span class="title">Activity</span>
                                    </a>
                                    <!-- Dropdown level 1 -->
                                    <div id="dropdown-form" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul class="nav navbar-nav">
                                                <li><a href="<?php echo BASE_URL;?>admin/activity">Request</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <!-- Dropdown-->
                                 <li class="panel panel-default dropdown">
                                    <a data-toggle="collapse" href="#component-example">
                                        <span class="icon fa fa-cubes"></span><span class="title">Emergency</span>
                                    </a>
                                    <!-- Dropdown level 1 -->
                                    <div id="component-example" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul class="nav navbar-nav">
                                                <!-- <li><a href="components/pricing-table.html">Pricing Table</a>
                                                </li> -->
                                                <li><a href="<?php echo BASE_URL;?>admin/emergency">Listing</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <!-- Dropdown-->
                                 <li class="panel panel-default dropdown">
                                    <a data-toggle="collapse" href="#dropdown-example">
                                        <span class="icon fa fa-slack"></span><span class="title">Event Management</span>
                                    </a>
                                    <!-- Dropdown level 1 -->
                                    <div id="dropdown-example" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul class="nav navbar-nav">
                                                <li><a href="<?php echo BASE_URL;?>admin/event">Event Details</a>
                                                </li>
                                               <!--  <li><a href="<?php echo BASE_URL;?>admin/event/addevent/">Event manage</a>
                                                </li> -->
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <!-- Dropdown-->
                              <!--   <li class="panel panel-default dropdown">
                                    <a data-toggle="collapse" href="#dropdown-icon">
                                        <span class="icon fa fa-archive"></span><span class="title">Icons</span>
                                    </a> -->
                                    <!-- Dropdown level 1 -->
                                   <!--  <div id="dropdown-icon" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul class="nav navbar-nav">
                                                <li><a href="icons/glyphicons.html">Glyphicons</a>
                                                </li>
                                                <li><a href="icons/font-awesome.html">Font Awesomes</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li> -->
                                <!-- <li>
                                    <a href="license.html">
                                        <span class="icon fa fa-thumbs-o-up"></span><span class="title">License</span>
                                    </a>
                                </li> -->
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </nav>
                </div>
                <?= $this->fetch('content'); ?>
                <?= $this->element('footer');?>
            <div>
        </div>
    </body>
</html>
