<!DOCTYPE html>
<html>
    <?= $this->element('index_header');?>
    <body>
		<div class="header" id="home">
			<div class="header-top">
				<div class="container">
					<p class="blinky"><span class="glyphicon glyphicon-calendar"></span> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
				</div>
			</div>
			<div class="header_nav" id="home">
				<nav class="navbar navbar-default chn-gd">
					<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand logo-st" href="index.html">Blood Share</a>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="<?php echo BASE_URL;?>">
								<span class="glyphicon glyphicon-home"></span> <br> Home
							</a>
						</li>
						<li>
						<a href="#" data-toggle="modal" data-target="#emergencyModal">
						<span class="glyphicon glyphicon-fire" aria-hidden="true"></span><br>
						Emergency
						</a>
						</li>
						<!---->
						<li>
						<a href="https://play.google.com/store?hl=en">
						<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span><br>
						Download App
						</a>
						</li>
						<!---->
						<li>
						<a href="<?= BASE_URL."Index/viewevent"?>">
						<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span><br>
							Events
						</a>
						</li>
						<!---->
						<li>
						<a href="#" data-toggle="modal" data-target="#signModal">
						<span class="glyphicon glyphicon-user" aria-hidden="true"></span><br>
						Sign In
						</a>
						</li>
						<!---->
						<li>	
						<a href="#contact" class="scroll">
						<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><br>
						Contact
						</a>
						</li>
						<!--script-->
						<script type="text/javascript">
						jQuery(document).ready(function($) {
						$(".scroll").click(function(event){		
						event.preventDefault();
						$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
						});
						});
						</script>
						<!--script-->
					</ul>
					</div><!-- /.navbar-collapse -->
					<div class="clearfix"></div>
					</div><!-- /.container-fluid -->
				</nav>
			</div>
			
		</div>
		<?= $this->fetch('content'); ?>
		<div class="footer">
			<div class="container">
				<div class="footer-text">
				<h3><a href="index.html">Blood Share</a></h3>
				<p>Copyright &copy; 2016 </p>
				</div>
			</div>
		</div>
		<a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
		<!-- Modals -->
		<div id="forgetModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Recover Password</h4>
		      </div>
		      <div class="modal-body">
		        <div class="row">
		        	<div class="col-md-8 col-md-offset-2">
		        		<form>
						  <div class="form-group">
						    <label for="email">Email:</label>
						    <input type="email" class="form-control" id="email">
						  </div>
						  <div class="form-group text-right">
						  	<button type="submit" class="btn btn-success">Send Link</button>
						  </div>
						</form>
		        	</div>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
		<div id="signModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Sign In To Blood Share</h4>
		      </div>
		      <div class="modal-body">
		        <div class="row">
		        	<div class="col-md-12 event-full">
		        		<div>
						  <!-- Nav tabs -->
						  <ul class="nav nav-tabs nav-justified" role="tablist">
						    <li role="presentation" class="active">
						    	<a href="#login" aria-controls="home" role="tab" data-toggle="tab">
						    		Login 
						    	</a>
						    </li>
						    <li role="presentation">
						    	<a href="#sup" aria-controls="profile" role="tab" data-toggle="tab">
						    		Sign Up
						    	</a>
						    </li>
						  </ul>
						  <!-- Tab panes -->
						  <div class="tab-content">
						    <div role="tabpanel" class="tab-pane active" id="login">
						    	<div class="row">
						    		<div class="col-md-8 col-md-offset-2">
						    			<form>
										  <div class="form-group">
										    <label for="User-name">User Name:</label>
										    <input type="text" class="form-control">
										  </div>
										  <div class="form-group">
										    <label for="pwd">Password:</label>
										    <input type="password" class="form-control">
										  </div>
										  <div class="form-group">
										  	<button type="submit" class="btn btn-success btn-block">Sign In</button>
										  </div>
										  <div class="form-group">
										  	<div class="row">
										  		<div class="col-md-6 col-sm-12">
										  			<button type="submit" class="btn btn-info btn-block">Facebook Login</button>
										  		</div>
										  		<div class="col-md-6 col-sm-12">
										  			<button type="submit" class="btn btn-danger btn-block">Google+ Login</button>
										  		</div>
										  		<div class="col-sm-12 text-right">
										  		<br>
										  			<a href="#" data-toggle="modal" data-dismiss="modal" data-target="#forgetModal">
										  				<small>Forgot Password?</small>
										  			</a>
										  		</div>
										  	</div>
										  </div>
										</form>
						    		</div>
						    	</div>
						    </div>
						    <div role="tabpanel" class="tab-pane" id="sup">
						    	<div class="row">
						    		<div class="col-md-8 col-md-offset-2">
						    			<form>
										  <div class="form-group">
										    <label for="User-name">User Name:</label>
										    <input type="text" class="form-control">
										  </div>
										  <div class="form-group">
										    <label for="email">Email Id:</label>
										    <input type="text" class="form-control">
										  </div>
										  <div class="form-group">
										    <label for="phone">Phone:</label>
										    <input type="number" class="form-control">
										  </div>
										  <div class="form-group">
										    <label for="pwd">Password:</label>
										    <input type="password" class="form-control">
										  </div>
										  <div class="form-group">
										  	<button type="submit" class="btn btn-success btn-block">Sign Up</button>
										  </div>
										  <div class="form-group">
										  	<div class="row">
										  		<div class="col-md-6 col-sm-12">
										  			<button type="submit" class="btn btn-info btn-block">Facebook Signup</button>
										  		</div>
										  		<div class="col-md-6 col-sm-12">
										  			<button type="submit" class="btn btn-danger btn-block">Google+ Signup</button>
										  		</div>
										  		<div class="col-sm-12 text-right">
										  		<br>
										  			<a href="#" data-toggle="modal" data-dismiss="modal" data-target="#forgetModal">
										  				<small>Forgot Password?</small>
										  			</a>
										  		</div>
										  	</div>
										  </div>
										</form>
						    		</div>
						    	</div>
						    </div>
						  </div>
						</div>
		        	</div>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
		<div id="emergencyModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Need Blood Urgently?</h4>
		      </div>
		      <div class="modal-body">
		        <div class="row">
		        	<div class="col-md-6 col-sm-12">
						  <div class="form-group">
						    <label for="User-name">Name:</label>
						    <input type="text" class="form-control">
						  </div>
		        	</div>
		        	<div class="col-md-6 col-sm-12">
		        		<div class="form-group">
						    <label for="phone">Phone:</label>
						    <input type="number" class="form-control">
						</div>
		        	</div>
		        	<div class="col-md-6 col-sm-12">
		        		<div class="form-group">
					  		<label>Choose Blood Group:</label>
						    <select class="form-control">
						    	<option value="">Select Blood Group</option>
								<?php foreach ($group as $value) { ?>
                                        <option value="<?php echo $value['group_name'];?>"><?php echo ucwords($value['group_name']); ?></option>
                                        <?php } ?>
						    </select>
					  	</div>
		        	</div>
		        	<div class="col-md-6 col-sm-12">
		        		<div class="form-group">
						  	<label>Choose Area:</label>
						    <select class="form-control">
						    	<option value="">Select Area</option>
								<?php foreach ($area as $value) { ?>
                                        <option value="<?php echo $value['name']; ?>"><?php echo ucwords($value['name']); ?></option>
                                        <?php } ?>
						    </select>
						</div>
		        	</div>
		        	<div class="col-md-6 col-sm-12">
		        		<div class="form-group">
						  	<label>Select Date Required:</label>
						    <input type="date" class="form-control">
						</div>
		        	</div>
		        	<div class="col-sm-12">
		        		<div class="form-group">
		        			<label>Description:</label>
		        			<textarea rows="2" class="form-control"></textarea>
		        		</div>
		        	</div>
		        	<div class="col-sm-12">
		        		<div class="form-group">
						  	<button type="submit" class="btn btn-success" onclick="window.location.href='search-results.html'">Search Donor</button>
						 </div>
		        	</div>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
	</body>
    </html>