<!DOCTYPE html>
<html>
<head>
    <title>Admin Login :: Blood Donation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <!-- CSS Libs -->
    <?= $this->Html->css('admin/bootstrap.min.css'); ?>
    <?= $this->Html->css('admin/font-awesome.min.css'); ?>
    <?= $this->Html->css('admin/animate.min.css'); ?>
    <?= $this->Html->css('admin/bootstrap-switch.min.css'); ?>
    <?= $this->Html->css('admin/checkbox3.min.css'); ?>
    <?= $this->Html->css('admin/jquery.dataTables.min.css'); ?>
    <?= $this->Html->css('admin/dataTables.bootstrap.css'); ?>
    <?= $this->Html->css('admin/select2.min.css'); ?>
    <!-- CSS App -->
    <?= $this->Html->css('style.css'); ?>
    <?= $this->Html->css('themes/flat-blue.css'); ?>
</head>
<body class="flat-blue login-page">
    <div class="container">
        <div class="login-box">
            <div>
                <div class="login-form row">
                    <div class="col-sm-12 text-center login-header">
                        <i class="login-logo fa fa-connectdevelop fa-5x"></i>
                        <h4 class="login-title">Blood Donation</h4>
                        <?= $this->Flash->render(); ?>
                    </div>
                    <div class="col-sm-12">
                        <div class="login-body">
                            <div class="progress hidden" id="login-progress">
                                <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                    Log In...
                                </div>
                            </div>
                            <form method="post" action="">
                                <div class="control">
                                    <label for="">User Name</label>
                                    <input type="text" name="username" class="form-control" placeholder="Username" />
                                </div>
                                <div class="control">
                                    <label for="">Password</label>
                                    <input type="password" placeholder="password" name="password" class="form-control" />
                                </div>
                                <div class="login-button text-center">
                                    <input type="submit" class="btn btn-primary" value="Login">
                                </div>
                            </form>
                        </div>
                        <div class="login-footer">
                            <span class="text-right"><a href="#" class="color-white">Forgot password?</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Javascript Libs -->
    <?= $this->Html->script('admin/jquery.min.js');?>
    <?= $this->Html->script('admin/bootstrap.min.js');?>
    <?= $this->Html->script('admin/Chart.min.js');?>
    <?= $this->Html->script('admin/bootstrap-switch.min.js');?>
    <?= $this->Html->script('admin/jquery.matchHeight-min.js');?>
    <?= $this->Html->script('admin/jquery.dataTables.min.js');?>
    <?= $this->Html->script('admin/dataTables.bootstrap.min.js');?>
    <?= $this->Html->script('admin/select2.full.min.js');?>
    <?= $this->Html->script('admin/ace/ace.js');?>
    <?= $this->Html->script('admin/ace/mode-html.js');?>
    <?= $this->Html->script('admin/ace/theme-github.js');?>
    <!-- Javascript -->
     <?= $this->Html->script('app.js');?>
</body>

</html>
