<head>
		<title>Home :: Blood Share</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content=""/>
		<!--fonts-->
		<link href='http://fonts.googleapis.com/css?family=Monda:400,700' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,100,700' rel='stylesheet' type='text/css'>
		<!--fonts-->
		<!--owlcss-->
		<?= $this->Html->css('owl.carousel.css'); ?>
		<!--bootstrap-->
		<?= $this->Html->css('bootstrap.min.css');?>
		<!--coustom css-->
		<?= $this->Html->css('style1.css');?>
		<!--default-js-->
		<?= $this->Html->script('jquery-2.1.4.min.js');?>
		<!--bootstrap-js-->
		<?= $this->Html->script('bootstrap.min.js');?>
		<!--script-->
		<?= $this->Html->script('move-top.js');?>
		<?= $this->Html->script('easing.js');?>
		<!--script-->
	</head>