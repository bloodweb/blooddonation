<head>
    
    <title> :: Blood Donation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <!-- CSS Libs -->
    <?= $this->Html->css('admin/bootstrap.min.css'); ?>
    <?= $this->Html->css('admin/font-awesome.min.css'); ?>
    <?= $this->Html->css('admin/animate.min.css'); ?>
    <?= $this->Html->css('admin/bootstrap-switch.min.css'); ?>
    <?= $this->Html->css('admin/checkbox3.min.css'); ?>
    <?= $this->Html->css('admin/jquery.dataTables.min.css'); ?>
    <?= $this->Html->css('admin/dataTables.bootstrap.css'); ?>
    
    <!-- CSS App -->
    <?= $this->Html->css('style.css'); ?>
    <?= $this->Html->css('themes/flat-blue.css'); ?>
</head>