<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
 <footer class="app-footer">
    <div class="wrapper">
        <span class="pull-right">
        	All rights reserved <a href="#"><i class="fa fa-long-arrow-up"></i></a>
        </span> 
        Blood Donation © 2016 Copyright.
    </div>
</footer>
<!-- Javascript Libs -->
<?= $this->Html->script('admin/jquery.min.js');?>
<?= $this->Html->script('admin/bootstrap.min.js');?>
<?= $this->Html->script('admin/Chart.min.js');?>
<?= $this->Html->script('admin/bootstrap-switch.min.js');?>
<?= $this->Html->script('admin/jquery.matchHeight-min.js');?>
<?= $this->Html->script('admin/jquery.dataTables.min.js');?>
<?= $this->Html->script('admin/dataTables.bootstrap.min.js');?>

<?= $this->Html->script('admin/ace/ace.js');?>
<?= $this->Html->script('admin/ace/mode-html.js');?>
<?= $this->Html->script('admin/ace/theme-github.js');?>
<!-- Javascript -->
 <?= $this->Html->script('app.js');?>
 <?= $this->Html->script('index.js');?>
 <?= $this->Html->script('admin/plugins/jquery-validation/jquery.validate.min.js'); ?>
<?= $this->Html->script('admin/plugins/jquery-validation/additional-methods.min.js'); ?>
<?= $this->Html->script('admin/include.js'); ?>