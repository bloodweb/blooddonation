<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;

/**
 * Faqcats Controller
 *
 * @property \App\Model\Table\FaqcatsTable $Faqcats
 */
class LocationController extends AppController {

    // public $components = array('Paginator');
    public function initialize() {
        $this->viewBuilder()->layout('default');
        $this->loadmodel("Adminlogins");
        $this->loadmodel("Locations");
        $this->loadComponent('Paginator');
    }
    public function index() {
        $area = $this->Locations->find()->where(array("parent_id" => 0))->toArray();
        $location = $this->Locations->find()->where(array("parent_id >" => 0))->all()->toArray();
        
        $this->set(compact('area','location'));
    }
    public function addarea() {
        if ($this->request->is('post')) {
        $this->request->data['date']=date('Y-m-d');
        if($this->request->data('id'))
        {
            $area=$this->Locations->get($this->request->data('id'));
        }
         else{
            $area = $this->Locations->newEntity();
             }
             $this->request->data['parent_id']=0;
            $area = $this->Locations->patchEntity($area, $this->request->data);
            if ($this->Locations->save($area)) {
                return $this->redirect(['action' => 'index']);
                $this->Flash->success(__('The area has been Saved Successfully.'));
                
            } else {
                $this->Flash->error(__('The area could not be saved. Please, try again.'));
            }
        $this->set(compact('area'));
        $this->set('_serialize', ['area']);
        return $this->redirect(['action' => 'index']);
    }
           

        }

       /* $area = $this->Locations->newEntity();
        if ($id != null) {
            $area = $this->Locations->get($id);
        }
        if ($this->request->is('post')) {
            $this->request->data['date']=date('Y-m-d');
            $this->request->data['parent_id']=0;
            $area = $this->Locations->patchEntity($area, $this->request->data);
            if ($this->Locations->save($area)) {
                $this->Flash->success(__('The area has been Saved Successfully.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The area could not be saved. Please, try again.'));
            }
        $this->set(compact('area'));
        $this->set('_serialize', ['area']);
        return $this->redirect(['action' => 'index']);
    }
    */

public function edit($id='') {
        $id=$this->request->data['id'];
        $userarea = $this->Locations->get($id, [
        'contain' => []
        ]);
        //echo $userarea;exit;
        if (!empty($userarea)) {
             echo json_encode($userarea);
            // exit;
         }else{
             echo 0;
             exit;

        }
        $area = $this->Locations->find()->where(array("parent_id" => 0));
        $group = $this->Bloodgroups->find()->where(array('is_deleted' => 0));
        $location = $this->Locations->find()->where(array("parent_id >" => 0));
        $this->set(compact('userarea','area','group','location'));
        $this->set('_serialize', ['userarea'],['area'],['location'],['group']);

    }
    public function deletearea($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $area = $this->Locations->get($id);
        if ($this->Locations->updateAll(array('parent_id' => 0), array('id' => $id))) {
            //unlink(BASE_URL . "thumbimg/" . $service->sicon);
            $this->Flash->success(__('The area has been deleted.'));
        } else {
            $this->Flash->error(__('The area could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
public function addlocation() {
      
     if ($this->request->is('post')) {
        $this->request->data['date']=date('Y-m-d');
        if($this->request->data('id'))
        {

            $location=$this->Locations->get($this->request->data('id'));
            pr($location);
            exit;
            
        }
         else{
            $location = $this->Locations->newEntity();

             }

            $this->request->data['parent_id']=0;
            $location = $this->Locations->patchEntity($location, $this->request->data);

            if ($this->Locations->save($location)) {
                return $this->redirect(['action' => 'index']);
                $this->Flash->success(__('The location has been Saved Successfully.'));
                
            } else {
                $this->Flash->error(__('The location could not be saved. Please, try again.'));
            }
        // $location = $this->Locations->find()->where(array("parent_id >" => 0))->all()->toArray();
        $this->set(compact('location'));
        $this->set('_serialize', ['location']);
        return $this->redirect(['action' => 'index']);



      // $location = $this->Locations->newEntity();
      //   if ($id != null) {
      //       $location = $this->Locations->get($id);
      //   }
      //   if ($this->request->is('post')) {
      //       $this->request->data['date']=date('Y-m-d');
      //       $this->request->data['parent_id'] >= 0;
      //       $location = $this->Locations->patchEntity($location, $this->request->data);
      //       if ($this->Locations->save($location)) {
      //          // $this->Flash->success(__('The location has been Saved Successfully.'));
      //           return $this->redirect(['action' => 'index']);
      //       } else {
      //           $this->Flash->error(__('The location could not be saved. Please, try again.'));
      //       }
      //   $this->set(compact('location'));
      //   $this->set('_serialize', ['location']);
      //   return $this->redirect(['action' => 'index']);
    }
}
public function EditArea(){  
        $id=$this->request->data['id'];
        $location = $this->Locations->get($id);
        if (!empty($location)) {
            echo json_encode($location);
            exit;
        }else{
            echo 0;
            exit;
        }
    }
    public function EditLocation(){  

          $id=$this->request->data['id'];
          $location = $this->Locations->find()->where(array("parent_id >" => 0,'id'=>$id))->first();
        if (!empty($location)) {
            echo json_encode($location);
            exit;
        }else{
            echo 0;
            exit;
        }
    }





   /* $id=$this->request->data['id'];
    //$area = $this->Locations->find()->where(array("parent_id" => 0));
    $location = $this->Locations->find()->where(array("parent_id >" => 0,'id'=>$id))->first();
    //$locationlist = $this->Locations->get($id);
        if (!empty($location)) {
            echo json_encode($location);
            exit;
        }else{
            echo 0;
            exit;
        }
    }*/
}

?>