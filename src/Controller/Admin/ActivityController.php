<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;

/**
 * Faqcats Controller
 *
 * @property \App\Model\Table\FaqcatsTable $Faqcats
 */
class ActivityController extends AppController {

    public function initialize() {
        $this->viewBuilder()->layout('default');
        $this->loadmodel("Users");
        $this->loadmodel("Bloodgroups");
        $this->loadComponent('Paginator');
    }
    public function index() {
        
    }
    public function activity() {
        
    }
}

?>