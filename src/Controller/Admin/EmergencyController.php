<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;


/**
 * Faqcats Controller
 *
 * @property \App\Model\Table\FaqcatsTable $Faqcats
 */
class EmergencyController extends AppController {

    public function initialize() {
        $this->viewBuilder()->layout('default');
        $this->loadmodel("Users");
        $this->loadmodel("Locations");
        $this->loadmodel("Bloodgroups");
        $this->loadmodel("Emergencyreqs");
        $this->loadComponent('Paginator');
    }
    public function index() {
    	$conditions = array();
    	$this->paginate = array('limit' => 10);
        $query = $this->Emergencyreqs->find()->where(array('is_deleted' => 0));
        $query->where($conditions)->all();
        $data = $this->paginate($query);
        $area = $this->Locations->find()->where(array("parent_id" => 0));
        $group = $this->Bloodgroups->find()->where(array('is_deleted' => 0));
        $location = $this->Locations->find()->where(array("parent_id >" => 0));
        $this->set(compact('data', 'group','area','location'));
        $this->set('_serialize', ['data'],['group'], ['area'],['location']);
    }
    public function add(){
        if ($this->request->is('post')) {
            if($this->request->data('id'))
            {
            $req=$this->Emergencyreqs->get($this->request->data('id'));
            }else{
            $req = $this->Emergencyreqs->newEntity();
             }
            $this->request->data['add_date']=date('Y-m-d');
            $req = $this->Emergencyreqs->patchEntity($req, $this->request->data);
            if ($this->Emergencyreqs->save($req)) {
                if ($id!='') {
                    return $this->redirect(['action' => 'index']);
                    $this->Flash->set('The data has been saved.', ['element' => 'success']);
                } else {
                    return $this->redirect(['action' => 'index']);
                    $this->Flash->success(__('The request has been Saved Successfully.'));
                }
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The request could not be saved. Please, try again.'));
            }
        $this->set(compact('req'));
        $this->set('_serialize', ['req']);
        return $this->redirect(['action' => 'index']);
    }

    }
    public function editreq($id='') {
        $id=$this->request->data['id'];
        $userlist = $this->Users->get($id, [
        'contain' => []
        ]);
        if (!empty($userlist)) {
            echo json_encode($userlist);
            exit;
        }else{
            echo 0;
            exit;

        }
    }
    public function selectlocation(){
        $id=$this->request->data['id'];
    	$area = $this->Locations->find()->where(array("parent_id" => 0,'id'=>$id))->all()->toArray();
    	foreach ($area as $k => $loc) {
        $id_area=$loc['id'];
        $res = $this->Locations->find()->where(array('parent_id'=>$id_area));
        $str="<option value=''>--Choose Location--</option>";
        foreach($res as $val){
            $str.="<option value='".$val['id']."'>".$val['name']."</option>";
        }
        echo $str;
        exit;
    }
}
public function emergencynotification(){
        if ($this->request->is('post')) {
            $data=$this->request->data;
            $msg = $data['notification'];
                    $title = $data['title'];
                    $req_id = $data['id'];
                    $req = $this->Emergencyreqs->find()->where(array('is_deleted' => 0))->first();
                    $bld=$req['blood_group'];
                    $query = $this->Users->find()->where(array('blood_group'=>$bld))->all()->toArray();
                    if (!empty($query)) {
                        $i = 0;
                        foreach ($query as $kd) {
                            $userData[$i] = $kd['blood_group'];
                            $i++;
                        }
                        // $this->send_notification($userData, $msg, $title, $type = "emergency", $req_id);
                         return $this->redirect(['action' => 'index']);
            
        }
        }
    }
}

?>