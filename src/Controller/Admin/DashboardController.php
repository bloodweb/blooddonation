<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;

/**
 * Faqcats Controller
 *
 * @property \App\Model\Table\FaqcatsTable $Faqcats
 */
class DashboardController extends AppController {

    public function beforeFilter(Event $event) {
        $this->viewBuilder()->layout('default');
        $this->loadmodel("Adminlogins");
    }
    public function isAuthorized($user) {
        return true;
    }
    public function index() {
       $this->viewBuilder()->layout('loginlayout');
        $name = $this->request->session()->read('username');
        if (!empty($name)) {
            $this->viewBuilder()->layout('loginlayout');
            return $this->redirect(['controller' => 'admin', 'action' => 'index']);
        }
        if ($this->request->is("post")) {
            // $username = $this->request->data("username");
            // $password = md5($this->request->data("password"));
            // $useradata = $this->Adminlogins->find()->where(array("username" => $username, "password" => $password))->first();
            // if (!empty($useradata)) {
            //     $session = $this->request->session();
            //     $session->write('username', $username);
            //     $session->write('userid', $useradata['id']);
            //     return $this->redirect(['action' => 'dashboard']);
            // } else {
            //     $this->Flash->error(__('Invalid User Id and password.'));
            //     return $this->redirect(['action' => 'index']);
            // }
            
            if ($this->request->is('post')) {
            $admin = $this->Auth->identify();
            if ($admin) {
                $this->Auth->setUser($admin);
                return $this->redirect($this->Auth->redirectUrl());
            }else{
            $this->Flash->error(__('Invalid username or password, try again'));
            return $this->redirect(['controller'=>'Dashboard','action'=>'index']);
         }
        }

        } else {
            $this->viewBuilder()->layout('loginlayout');
        }
    }

    public function checkpassword() {
        $data=$this->Adminlogins->find()->all()->toArray();
        $userid = $data[0]['id'];
        $this->set(compact('userid'));
        if ($this->request->is('post')) {
            $password = md5($this->request->data['password']);
            if (!empty($password)) {
                $query=$this->Adminlogins->find()->where(['password' => $password], ['id' => $userid])->count();
                if ($query == 0) {
                    echo 0;
                } else {
                    echo 1;
                    exit;
                }
            }
        }
    }

    public function changepassword() {
        if ($this->request->is('post')) {
            $userid = $this->request->data['userid'];
            $password = md5($this->request->data['password']);
            if ($this->Adminlogins->updateAll(array("password" => $password), array('id' => $userid))) {
                $this->Flash->success(__('The password has been changed.'));
            } else {
                $this->Flash->error(__('The password could not be saved. Please, try again.'));
            }
            return $this->redirect(['action' => 'checkpassword']);
        }
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }
    public function dashboard() {

    }
    public function adminprofile($id='') {
        $data = $this->Adminlogins->newEntity();
        if ($id != null) {
            $data = $this->Adminlogins->get($id);
        }
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $file = $this->request->data['profile'];
            $dir = WWW_ROOT . 'images' . DS;
            $filename = $file['name'];
            $dir = $dir . $filename;
            if (move_uploaded_file($file['tmp_name'], $dir)) {
                $data['profile'] = $filename;
            } else {
                $data['profile'] = $data['prev_img'];
            }
            $data = $this->Adminlogins->patchEntity($data, $this->request->data);
            if ($this->Adminlogins->save($data, array('id' => $id))) {
                $this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong></strong>The settings has been saved</div>'));
            } else {
                $this->Session->setFlash(__('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button><strong></strong>The setting could not saved</div>'));
            }
            $this->redirect(array("controller" => "dashboard", "action" => "dashboard"));
        }
        $admin = $this->Adminlogins->find()->first();
        $this->set('admin', $admin);
    }

}

?>