<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;

/**
 * Faqcats Controller
 *
 * @property \App\Model\Table\FaqcatsTable $Faqcats
 */
class EventController extends AppController {

    public function initialize() {
        $this->viewBuilder()->layout('default');
        $this->loadmodel("Users");
        $this->loadmodel("Locations");
        $this->loadmodel("Bloodgroups");
        $this->loadmodel("Events");
        $this->loadComponent('Paginator');
    }
    public function index() 
    {
       $events = $this->Events->find()->where(array("is_deleted" => 0))->toArray();  
        //$area = $this->Locations->find()->where(array("parent_id" => 0))->first();
        $location = $this->Locations->find()->where(array("parent_id >" => 0))->first();
        $areas = $this->Locations->find()->where(array("parent_id" => 0))->all()->toArray();
        $area = '';
        foreach ($areas as $val) {
            $area[$val['id']] = $val['name'];
        }
        $this->set('areas', $area);

        $this->set(compact('area','location','events'));
    }
    public function addevent() 
    {
        if ($this->request->is('post')) {


            if($this->request->data('id'))
            {
            $req=$this->Events->get($this->request->data('id'));
            }else{
            $req = $this->Events->newEntity();
             }
            $this->request->data['add_date']=date('Y-m-d');
              if (!empty($this->request->data['eventbanner']['name'])) {
                $file = $this->request->data['eventbanner']['name'];

                $ext = substr(strtolower(strrchr($this->request->data['eventbanner']['name'], '.')), 1); //get the extension

                
                $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
            
                if (in_array($ext, $arr_ext)){
                    $image_path = WWW_ROOT . 'img/' . $this->request->data['eventbanner']['name'];
                    $thumb_path = WWW_ROOT . 'thumbimg/' .$this->request->data['eventbanner']['name'];
                    if (move_uploaded_file($this->request->data['eventbanner']['tmp_name'], $image_path)) {
                        //$this->createthumb($image_path, $thumb_path, 100, 100);
                        $this->request->data['eventbanner'] =$this->request->data['eventbanner']['name'];
                    }
                }
            $req = $this->Events->patchEntity($req, $this->request->data);
            if ($this->Events->save($req)) {

                    return $this->redirect(['action' => 'index']);
                    $this->Flash->set('The data has been saved.', ['element' => 'success']);
                } else {
                    return $this->redirect(['action' => 'index']);
                    $this->Flash->success(__('The request has been Saved Successfully.'));
                }
                return $this->redirect(['action' => 'index']);
            } else {
                //$this->Flash->error(__('The request could not be saved. Please, try again.'));
            }
        $this->set(compact('req'));
        $this->set('_serialize', ['req']);
        return $this->redirect(['action' => 'index']);
    }
}
}

?>