<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;

/**
 * Faqcats Controller
 *
 * @property \App\Model\Table\FaqcatsTable $Faqcats
 */
class UserController extends AppController {

    public function initialize() {
        $this->viewBuilder()->layout('default');
        $this->loadmodel("Users");
        $this->loadmodel("Bloodgroups");
        $this->loadmodel("Locations");
        $this->loadComponent('Paginator');
    }
    public function index() {
        $conditions = array();
        if (!empty($this->request->query['name'])) {
            $conditions[] = array('name LIKE ' => $this->request->query['name']."%");
        }
        if (!empty($this->request->query['area'])) {
            $conditions[] = array('area LIKE ' => $this->request->query['area']."%");
        }
        if (!empty($this->request->query['location'])) {
            $conditions[] = array('location LIKE ' => $this->request->query['location']."%");
        }
        $this->paginate = array('limit' => 10);
        $query = $this->Users->find()->where(array('is_deleted' => 0));
        $query->where($conditions)->all();
        $user = $this->paginate($query);
        $area = $this->Locations->find()->where(array("parent_id" => 0));
        $group = $this->Bloodgroups->find()->where(array('is_deleted' => 0));
        $location = $this->Locations->find()->where(array("parent_id >" => 0));
        $this->set(compact('user', 'group','area','location'));
        $this->set('_serialize', ['user'],['group'], ['area'],['location']);
    }
    public function adduser() {
        if ($this->request->is('post')) {
            if($this->request->data('id'))
        {
            $user=$this->Users->get($this->request->data('id'));
        }else{
            $user = $this->Users->newEntity();
             }
            $this->request->data['add_date']=date('Y-m-d');
            $this->request->data['password']=isset($user['password']) ? $user['password'] : "12345";
            $user = $this->Users->patchEntity($user, $this->request->data);
            if (empty($user['userid'])) {
            $user['userid'] = $this->getuserid();
            if ($this->userSave($user,$this->request->data)) {
                if (empty($user['userid'])) {
                $lastID = $user->id;
                $userid = "BLD00" . $lastID;
                $this->Users->updateAll(array('id' => $lastID));
                } else {
                $lastID = $user->id;
            }
                    if ($id!='') {
                    return $this->redirect(['action' => 'bloodgroup']);
                    $this->Flash->set('The data has been saved.', ['element' => 'success']);
                } else {
                    return $this->redirect(['action' => 'bloodgroup']);
                    $this->Flash->success(__('The user has been Saved Successfully.'));
                }
                return $this->redirect(['action' => 'bloodgroup']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
        return $this->redirect(['action' => 'index']);
    }
}
}
    public function getuserid() {
        $this->loadmodel("Users");
        $lastCreated = $this->Users->find()->order(array("id" => "DESC"))->first();
        if (empty($lastCreated)) {
            $id = 1;
        } else {
            $id = (int) $lastCreated['id'] + 1;
        }
        return "BLD00" . $id;
    }
    public function edit($id='') {
        $id=$this->request->data['id'];
        $userlist = $this->Users->get($id, [
        'contain' => []
        ]);
        if (!empty($userlist)) {
            echo json_encode($userlist);
            exit;
        }else{
            echo 0;
            exit;

        }
    }
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->updateAll(array('is_deleted' => 1), array('id' => $id))) {
            return $this->redirect(['action' => 'index']);
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
     public function changestatus() {
        $status = $this->request->data('status');
        $id = $this->request->data('id');
        if ($this->Users->updateAll(array('status' => $status), array('id' => $id))) {
            echo $status;
        }
        exit;
    }

    public function blood() {
        if ($this->request->is('post')) {
            if($this->request->data('id'))
            {
            $blood=$this->Bloodgroups->get($this->request->data('id'));
            }else{
            $blood = $this->Bloodgroups->newEntity();
             }
            $this->request->data['date']=date('Y-m-d');
            $blood = $this->Bloodgroups->patchEntity($blood, $this->request->data);
            if ($this->Bloodgroups->save($blood)) {
                if ($id!='') {
                    return $this->redirect(['action' => 'bloodgroup']);
                    $this->Flash->set('The data has been saved.', ['element' => 'success']);
                } else {
                    return $this->redirect(['action' => 'bloodgroup']);
                    $this->Flash->success(__('The bloodgroup has been Saved Successfully.'));
                }
                return $this->redirect(['action' => 'bloodgroup']);
            } else {
                $this->Flash->error(__('The Users could not be saved. Please, try again.'));
            }
        $this->set(compact('blood'));
        $this->set('_serialize', ['blood']);
        return $this->redirect(['action' => 'bloodgroup']);
    }
}
    public function editgroup($id='') {
        $id=$this->request->data['id'];
        $bldgroup = $this->Bloodgroups->get($id, [
        'contain' => []
        ]);
        if (!empty($bldgroup)) {
            echo json_encode($bldgroup);
            exit;
        }else{
            echo 0;
            exit;

        }
    }
    public function bloodgroup(){
        $this->paginate = array('limit' => 20);
        $query = $this->Bloodgroups->find()->where(array('is_deleted' => 0));
        $blood = $this->paginate($query);
        $this->set(compact('blood'));
        $this->set('_serialize', ['blood']);
 }
 public function bldgrpdelete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $group = $this->Bloodgroups->get($id);
        if ($this->Bloodgroups->updateAll(array('is_deleted' => 1), array('id' => $id))) {
            //unlink(BASE_URL . "thumbimg/" . $service->sicon);
            return $this->redirect(['action' => 'bloodgroup']);
            $this->Flash->success(__('The data has been deleted.'));
        } else {
            $this->Flash->error(__('The data could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'bloodgroup']);
    }
    public function adduserid(){
        $lastCreated = $this->Users->find()->order(array("id" => "DESC"))->first();
        if (empty($lastCreated)) {
            $id = 1;
        } else {
            $id = (int) $lastCreated['id'] + 1;

        }
        $uid="BLD00" . $id;
        echo $uid;
        exit;

    }

    public function useractivity() {        
    }
    public function userating() {        
    }
}

?>