<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use MyLibraryBaseClass;
use Cake\Controller\Component\AuthComponent;
use Cake\Controller\Controller;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use App\Controller\Component;
use phpmailer;
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Cookie');
        $this->loadComponent('Paginator');
        // $this->loadComponent('Email', [
        // 'className' => 'Email']);
        if($this->request->prefix=='admin'){
        $this->loadComponent('Auth', [
        'authorize' => ['Controller'],
        'loginAction' => [
            'controller' => 'dashboard',
            'action' => 'index'
        ],
        'authenticate' => [
                          'Form' => [
                            'userModel' => 'Adminlogins', // Added This
                            'fields' => [
                              'username' => 'username',
                              'password' => 'password',
                             ]
                           ]
                     ],   
        'loginRedirect' => [
            'controller' => 'Dashboard',
            'action' => 'dashboard'
        ],
        'logoutRedirect' => [
            'controller' => 'dashboard',
            'action' => 'index'
        ]
    ]);
        $this->Auth->allow('index');

    }else {
        $this->loadComponent('Auth', [
        'authorize' => ['Controller'],
        'loginAction' => [
            'controller' => 'admin',
            'action' => 'index'
        ],
        'authenticate' => [
                          'Form' => [
                            'userModel' => 'Admins', // Added This
                            'fields' => [
                              'username' => 'username',
                              'password' => 'password',
                             ]
                           ]
                     ],  
        'loginRedirect' => [
            'controller' => 'admin',
            'action' => 'dashboard'
        ],
        'logoutRedirect' => [
            'controller' => 'admin',
            'action' => 'index'
        ]

    ]);
        $this->Auth->allow();
     }
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

  public function send_notification($registatoin_ids, $message, $title='', $type='', $order_id='', $status='') {
        $url = 'https://android.googleapis.com/gcm/send';
        $arrychnk=array_chunk($registatoin_ids,1000);
        foreach($arrychnk as $regid){
        $fields = array(
            'registration_ids' => $regid,
            'data' => array("title" => $title, "description" => $message, "type" => $type, "order_id" => $order_id, "status" => $status)
        ); //print_r(json_encode($fields));
        $headers = array(
            'Authorization: key=AIzaSyAbjjBXIeki6zX5-lImT99spdq-kMTcr7Q',
            'Content-Type: application/json'
        );
// Open connection
        $ch = curl_init();

// Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

// Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
    }
        return 1;
       //echo $result;exit();
    }
    public function userSave($user) {
        $this->loadmodel("Users");
//================= Save users and wallet with transaction ============//
        if (empty($user['user_id'])) {
            $user['userid'] = $this->getuserid();
            $user['password'] = isset($user['password']) ? $user['password'] : "12345";
        }
         $user['name']=ucfirst($user['name']);
        if ($this->Users->save($user)) {
            if (empty($user['user_id'])) {
                $lastID = $user->id;
                $userid = "BLD00" . $lastID;
                $this->Users->updateAll(array('id' => $lastID));
            } else {
                $lastID = $user->id;
            }
            $res = $lastID;
        } else {
            $res = 0;
        }
        return $res;
    }
    public function getuserid() {
        $this->loadmodel("Users");
        $lastCreated = $this->Users->find()->order(array("id" => "DESC"))->first();
        if (empty($lastCreated)) {
            $id = 1;
        } else {
            $id = (int) $lastCreated['id'] + 1;
        }
        return "BLD00" . $id;
    }
  }