<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;

/**
 * Faqcats Controller
 *
 * @property \App\Model\Table\FaqcatsTable $Faqcats
 */
class MobileapiController extends AppController {

    public function initialize() {
        $this->viewBuilder()->layout('default');
        $this->loadmodel("Users");
        $this->loadmodel("Bloodgroups");
        $this->loadmodel("Locations");
        $this->loadComponent('Paginator');
    }

    /**
     * [login description : Manual Login by Android Users.....]
     * @return [JSON] [Status with users details......]
     * @author Dhiraj Mohapatra <[dhiraj@pcinfosolutions.com]>
     * @data("28-04-2016")
     */
     public function login() {
        if ($this->request->is('post')) {
            $username = $this->request->data['username'];
            $pwd = $this->request->data['password'];
            if ($username != '' && $pwd != '') {
                 $usernameCond = "mobile";
                $res = $this->Users->find()->where(array($usernameCond=>$username,'password'=>$pwd))->first();
                if (!empty($res)) {
                    if ($pwd == $res['password']) {
                        $arr['status'] = 1;
                        $arr['msg'] = "Successss";
                        $arr['data'] = $res;
                    }else{
                        $arr['status'] = 0;
                        $arr['msg'] = "Invalid username & password";
                    }
                } else {
                    $arr['status'] = 0;
                    $arr['msg'] = "Invalid username & password";
                }
            } else {
                $arr['status'] = 0;
                $arr['msg'] = "Insufficient Data";
            }
            echo json_encode($arr);
            exit;
        }
    }
     public function signup() {
        if ($this->request->is("post") && !empty($this->request->data)) {
            if ($this->request->data['email'] != "" && ($this->request->data['password'] != "" && $this->request->data['mobile'] != "" && $this->request->data['name'] != "")) {
                $email = trim($this->request->data['email']);
                $password = trim($this->request->data['password']);
                $fullname = trim($this->request->data['name']);
                $mobile = trim($this->request->data['mobile']);
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    if (is_numeric($mobile)) {
                        if (strlen($mobile) == 10) {
//================= Save users and wallet with transaction ============//
                            $user = $this->Users->newEntity();
                            $user = $this->Users->patchEntity($user, $this->request->data);
                            $saveStatus = $this->saveuser($user,$id = null, $this->request->data['fcm_id'],$this->request->data['login_type']);
                            if ($saveStatus == "1") {
                                $arr['status'] = 1;
                                $arr['data'] = "Mobile Number muyst be 10 digits..";
                                if(isset($this->request->data['version']) && isset($this->request->data['modelname'])){
                                  $this->Users->updateAll(array("version"=>$this->request->data['version'],'modelname'=>$this->request->data['modelname']),array("mobile"=>$this->request->data['mobile']));
                                  }
                                $this->Flash->success(__('The user has been saved.'));
                            } else {
                                $this->Flash->error(__('The user could not be saved. Please, try again.'));
                            }
//=====================================================================//
                        } else {
                            $arr['status'] = 0;
                            $arr['data'] = "Mobile Number muyst be 10 digits..";
                        }
                    } else {
                        $arr['status'] = 0;
                        $arr['data'] = "Invalid Mobile Number..";
                    }
                } else {
                    $arr['status'] = 0;
                    $arr['data'] = "Invalid Email-ID..";
                }
            }
        }
    }

     public function bloodgroups() {
        if ($this->request->is('post')) {
        $bldlist = $this->Bloodgroups->find()->where(array('is_deleted' => 0))->all()->toArray();
        foreach ($bldlist as $k => $list) {
            $data[$k]["Blood_group"] = $list["group_name"];
            $data[$k]["description"] = $list["description"];
        }
        $Bloodgroup = array("status" => 1,"msg"=>"Success", "data" => $data);
        echo json_encode($Bloodgroup);
        exit;
    }
}
public function arealist() {
        if ($this->request->is('post')) {
        $area = $this->Locations->find()->where(array("parent_id" => 0))->all()->toArray();
        foreach ($area as $k => $area) {
            $data[$k]["Area_id"] = $area["id"];
            $data[$k]["Area_name"] = $area["name"];
        }
        $Arealist = array("status" => 1,"msg"=>"Success", "data" => $data);
        echo json_encode($Arealist);
        exit;
    }
}
public function locationlist() {
        if (!empty($this->request->data('area_id'))) {
        $area = $this->Locations->find()->where(array("parent_id" => 0,'id'=>$this->request->data['area_id']))->all()->toArray();
        foreach ($area as $k => $loc) {
            $areaid=$loc['id'];
            $location=$this->Locations->find()->where(array("parent_id" => $areaid))->all()->toArray();
            if (!empty($location)) {
                foreach ($location as $key => $value) {
                    $data[$key]["id"] = $value["id"];
                    $data[$key]["name"] = $value["name"];
                }
            }

        }
        if(!empty($data)){
        $locationdata = array("status" => 1,"msg"=>"Success",'location'=>$data);
    }else{
        $locationdata = array("status" => 0,"msg"=>"No location found");
    }
        echo json_encode($locationdata);
        exit;
    }
}
public function searchrequest(){

}

public function sendotp() {
    if (!empty($this->request->data['user_id'])) {
        $getUserdetail = $this->Users->find()->where(array('userid' => $this->request->data['user_id']))->first();
        if (!empty($getUserdetail)) {
            if ($getUserdetail['mobile'] != '') {
                $otp = rand(100000, 999999);
                $otp_time = date("Y-m-d H:i:s");
                $msg = "Verification code is " . $otp . " for authentication of your mobile number. Regards Team Blooddonation.";
            $this->Users->updateAll(array('otp' => $otp, 'otp_time' => $otp_time), array('id' => $getUserdetail['id']));
                $mobile= trim($getUserdetail['mobile']);
        //$this->sendSms($mobile,$msg);
                $userData = array('status' => 1, 'data' => "OTP Send Successfully");              
            } else {
                $userData = array('status' => 400, 'data' => "Please Enter Mobile Number");
            }
        } else {
            $userData = array('status' => 2, 'data' => "Invalid User Id");
        }
    } else {
        $userData = array('status' => 0, 'data' => "Insufficient Data");
    }
    echo json_encode($userData);
    exit;
}

public function checkotp() {
    if (!empty($this->request->data['user_id']) && !empty($this->request->data['otp'])) {
        $getUserdetail = $this->Users->find()->where(array('userid' => $this->request->data['user_id']))->first();
        if (!empty($getUserdetail)) {
            if ($getUserdetail['otp'] != '') {
                $otp_gentime = strtotime('+5 minutes', strtotime($getUserdetail['otp_time']));
                if ($getUserdetail['otp'] == $this->request->data['otp']) {
                    $this->Users->updateAll(array('mobile_verified' => 1), array('id' => $getUserdetail['id']));
                    $getnewUserdetail = $this->Users->find()->where(array('userid' => $this->request->data['user_id']))->first();
                    $getnewUserdetail['mobile_verified'] =  $getnewUserdetail['mobile_verified'] != 0 ? 1 : 0;
                    $userData = array('status' => 1, 'data' => $getnewUserdetail);
                    if ($getUserdetail['mobile'] != '') {
                        //  $msg_mob = "Welcome to WASHDUDE, Now enjoy world class laundry experience. Please contact us 9776999222 anytime for more information";
                        // $this->sendSms($getUserdetail['mobile'], $msg_mob);
                    }
                } else {
                    $userData = array('status' => 4, 'data' => "Invalid OTP, Please Resend");
                }
            } else {
                $userData = array('status' => 3, 'data' => "You have not generate any otp");
            }
        } else {
            $userData = array('status' => 2, 'data' => "Invalid User Id");
        }
    } else {
        $userData = array('status' => 0, 'data' => "Insufficient Data");
    }
    echo json_encode($userData);
    exit;
}


}

?>

