<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;


/**
 * Faqcats Controller
 *
 * @property \App\Model\Table\FaqcatsTable $Faqcats
 */
class IndexController extends AppController {

    // public $components = array('Paginator');
    public function beforeFilter(Event $event) {
        $this->viewBuilder()->layout('indexlayout');
        $this->loadmodel("Bloodgroups");
        $this->loadmodel("Locations");
        $this->loadComponent('Paginator');
    }
    public function index() {
        $area = $this->Locations->find()->where(array("parent_id" => 0));
        $location = $this->Locations->find()->where(array("parent_id >" => 0));
        $group = $this->Bloodgroups->find()->where(array('is_deleted' => 0));
        $this->set(compact('area','location','group'));
        
    }
    public function viewevent() {
        
        
    }
    public function searchresult(){
        $area = $this->Locations->find()->where(array("parent_id" => 0));
        $location = $this->Locations->find()->where(array("parent_id >" => 0));
        $group = $this->Bloodgroups->find()->where(array('is_deleted' => 0));
        $this->set(compact('area','location','group'));

    }
    

}

?>